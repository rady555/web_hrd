﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace POS_Csharp
{
    public partial class frmLog_In : Form
    {
        public frmLog_In()
        {
            InitializeComponent();
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);

        SqlConnection Con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");

        protected override void WndProc(ref Message m)
        {
            const int WM_NCCALCSIZE = 0x0083;
            if (m.Msg == WM_NCCALCSIZE && m.WParam.ToInt32() == 1)
            {
                return;
            }
            base.WndProc(ref m);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Con.Open();
            //String q = ("SELECT userid, email, username, gender, password, address, active, phone, date_of_birth FROM sysuser");

            //if (txtUsername.Text == "blue" && txtPassword.Text == "123")
            //{
            //    new frmMainForm().Show();
            //    this.Hide();
            //}
            //else
            //{
            //    MessageBox.Show("The Username or Password is incorrect! Please try again!");
            //    txtUsername.Clear();
            //    txtPassword.Clear();
            //    txtUsername.Focus();
            //}
            string username = txtUsername.Text, password = txtPassword.Text;
            try
            {
                String sql = "Select * From sysuser Where email = '" + txtUsername.Text + "' and password = '" + txtPassword.Text + "';";
                SqlDataAdapter sdt = new SqlDataAdapter(sql, Con);
                DataTable dt = new DataTable();
                sdt.Fill(dt);

                if (dt.Rows.Count == 1)
                {
                    username = txtUsername.Text;
                    password = txtPassword.Text;
                    frmMainForm form = new frmMainForm();
                    form.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Invalid username of password!","Error",MessageBoxButtons.OK, MessageBoxIcon.Error);

                    txtUsername.Clear();
                    txtPassword.Clear();
                    txtUsername.Focus();
                }
            }
            catch(Exception ex)
            {

            }
            Con.Close();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                txtPassword.PasswordChar = '\0';
            }
            else
            {
                txtPassword.PasswordChar = '*';
            }
        }
    }
}
