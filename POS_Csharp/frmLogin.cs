﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace POS_Csharp.Forms
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }
        SqlConnection Con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Con.Open();
            String q = ("SELECT userid, email, username, gender, password, address, active, phone, date_of_birth FROM sysuser");

            if (txtUsername.Text == "blue" && txtPassword.Text == "123")
            {
                new frmMainForm().Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("The Username or Password is incorrect! Please try again!");
                txtUsername.Clear();
                txtPassword.Clear();
                txtUsername.Focus();
            }
            Con.Close();
        }
    }
}
