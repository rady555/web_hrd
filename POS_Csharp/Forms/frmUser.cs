﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace POS_Csharp.Forms
{
    public partial class frmUser : Form
    {
        public frmUser()
        {
            InitializeComponent();
            DisplayUsers();
            Count_rows();
        }

        SqlConnection Con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True");

        private void frmUser_Load(object sender, EventArgs e)
        {
            txtUserID.Text = " ";
            txtUsername.Text = "";
            txtEmail.Text = " ";
            txtPhone.Text = "";
            txtPassword.Text = "";
            cboGender.Text = "";
            txtAddress.Text = "";
            checkActive.Text = "";
            cboGender.Text = "";
            dateTimePicker1.Text = "";
        }
        private void DisplayUsers()
        {
            int count = 0;
            Con.Open();
            String q = ("SELECT userid, email, username, gender, password, address, active, phone, date_of_birth FROM sysuser");
            SqlCommand sqli = new SqlCommand(q, Con);
            SqlDataAdapter sql = new SqlDataAdapter(q, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();


            while (reader.Read())
            {
                dgv1.Rows.Add(reader["userid"], reader["email"], reader["username"], reader["gender"], reader["password"], reader["address"], reader["active"], reader["phone"], reader["date_of_birth"]);
                dgv1.RowHeadersVisible = false;
                count = dgv1.RowCount;
                lblCount.Text = count.ToString() + " records";
            }         
            Con.Close();
        }
        public int Count_rows()
        {
            int count = 0;
            Con.Open();
            string stmt = "SELECT COUNT(*) FROM dbo.sysuser";
            SqlCommand cmdCount = new SqlCommand(stmt, Con);
            count = (int)cmdCount.ExecuteScalar();
            Con.Close();
            lblCount.Text = count.ToString() + " records";
            return count;
        }

        private void Reset()
        {
            txtUserID.Text = "";
            txtEmail.Text = "";
            txtUsername.Text = "";
            cboGender.Text = "";
            txtPassword.Text = "";
            txtAddress.Text = "";
            checkActive.Checked = false;
            dateTimePicker1.Text = "";
            txtPhone.Text = "";
        }
        int Key = 0;
        private void dgv1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                txtUserID.Text = dgv1.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtEmail.Text = dgv1.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtUsername.Text = dgv1.Rows[e.RowIndex].Cells[2].Value.ToString();
                cboGender.Text = dgv1.Rows[e.RowIndex].Cells[3].Value.ToString();
                txtPassword.Text = dgv1.Rows[e.RowIndex].Cells[4].Value.ToString();
                txtAddress.Text = dgv1.Rows[e.RowIndex].Cells[5].Value.ToString();
                //checkActive.Text = dgv1.Rows[e.RowIndex].Cells[6].Value.ToString();
                txtPhone.Text = dgv1.Rows[e.RowIndex].Cells[7].Value.ToString();
                dateTimePicker1.Text = dgv1.Rows[e.RowIndex].Cells[8].Value.ToString();
                if (txtUserID.Text == "")
                {
                    Key = 0;
                }
                else
                {
                    Key = Convert.ToInt32(dgv1.Rows[e.RowIndex].Cells[0].Value.ToString());
                }
                if (dgv1.Rows[e.RowIndex].Cells[6].Value.ToString() == "1")
                {
                    checkActive.Checked = true;
                }
                else
                {
                    checkActive.Checked = false;
                }
            }
            else
            {
                Reset();
            }
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            new AddUser(dgv1).Show();
            //this.Hide();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtUserID.Text == "")
            {
                MessageBox.Show("Please select user!");
            }
            else
            {
                if (MessageBox.Show("Do you want to delete this user?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {

                    Con.Open();
                    SqlCommand cmd = new SqlCommand("delete FROM sysuser where userid = @userid", Con);
                    cmd.Parameters.AddWithValue("@userid", txtUserID.Text);

                    cmd.ExecuteNonQuery();
                    Con.Close();
                    Reset();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    MessageBox.Show("User Deleted!");
                    Refresh();
                    }
            }
        }

        private void Refresh()
        {
            Con.Open();
            String q = ("SELECT userid, email, username, gender, password, address, active, phone, date_of_birth FROM sysuser WHERE userid like'" + this.txtSearch.Text + "%' OR username like'" + this.txtSearch.Text + "%'");
            SqlCommand sqli = new SqlCommand(q, Con);
            SqlDataAdapter sql = new SqlDataAdapter(q, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            dgv1.Rows.Clear();
            while (reader.Read())
            {
                dgv1.Rows.Add(reader["userid"], reader["email"], reader["username"], reader["gender"], reader["password"], reader["address"], reader["active"], reader["phone"], reader["date_of_birth"]); ;
                dgv1.RowHeadersVisible = false;
            }
            Reset();
            Con.Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (txtUserID.Text == "")
            {
                MessageBox.Show("Please select user!");
            }
            else
            {
                SqlConnection Con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");
                SqlCommand cmd = new SqlCommand("update sysuser set userid=@userid, email=@email, username=@username, gender=@gender, password=@password, " +
                    "address=@address, date_of_birth=@date_of_birth, phone=@phone, active=@active where userid=@PKey;", Con);
                Con.Open();
                int active = 0;
                if (checkActive.Checked == true)
                {
                    active = 1;
                }
                cmd.Parameters.AddWithValue("@userid", txtUserID.Text);
                cmd.Parameters.AddWithValue("@email", txtEmail.Text);
                cmd.Parameters.AddWithValue("@username", txtUsername.Text);
                cmd.Parameters.AddWithValue("@gender", cboGender.Text);
                cmd.Parameters.AddWithValue("@password", txtPassword.Text);
                cmd.Parameters.AddWithValue("@address", txtAddress.Text);
                cmd.Parameters.AddWithValue("@date_of_birth", dateTimePicker1.Text);
                cmd.Parameters.AddWithValue("@phone", txtPhone.Text);
                cmd.Parameters.AddWithValue("@active", active);
                cmd.Parameters.AddWithValue("@PKey", Key);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                MessageBox.Show("User Updated!");
                Refresh();
                Con.Close();
            }
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            if (txtUserID.Text == "")
            {
                MessageBox.Show("Please select user!");
            }
            else
            {
                try
                {

                    Con.Open();
                    SqlCommand cmd = new SqlCommand("delete FROM sysuser where userid = @userid", Con);
                    cmd.Parameters.AddWithValue("@userid", txtUserID.Text);

                    cmd.ExecuteNonQuery();
                    Con.Close();
                    Reset();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                MessageBox.Show("User Deleted!");
                Refresh();
            }
        }

        private void dgv1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                txtUserID.Text = dgv1.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtEmail.Text = dgv1.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtUsername.Text = dgv1.Rows[e.RowIndex].Cells[2].Value.ToString();
                cboGender.Text = dgv1.Rows[e.RowIndex].Cells[3].Value.ToString();
                txtPassword.Text = dgv1.Rows[e.RowIndex].Cells[4].Value.ToString();
                txtAddress.Text = dgv1.Rows[e.RowIndex].Cells[5].Value.ToString();
                //checkActive.Text = dgv1.Rows[e.RowIndex].Cells[6].Value.ToString();
                txtPhone.Text = dgv1.Rows[e.RowIndex].Cells[7].Value.ToString();
                dateTimePicker1.Text = dgv1.Rows[e.RowIndex].Cells[8].Value.ToString();
                if (txtUserID.Text == "")
                {
                    Key = 0;
                }
                else
                {
                    Key = Convert.ToInt32(dgv1.Rows[e.RowIndex].Cells[0].Value.ToString());
                }
                if (dgv1.Rows[e.RowIndex].Cells[6].Value.ToString() == "1")
                {
                    checkActive.Checked = true;
                }
                else
                {
                    checkActive.Checked = false;
                }
            }
            else
            {
                Reset();
            }
        }

        private void btnEdit_Click_1(object sender, EventArgs e)
        {
            if (txtUserID.Text == "")
            {
                MessageBox.Show("Please select user!");
            }
            else
            {
                SqlConnection Con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");
                SqlCommand cmd = new SqlCommand("update sysuser set userid=@userid, email=@email, username=@username, gender=@gender, password=@password, " +
                    "address=@address, date_of_birth=@date_of_birth, phone=@phone, active=@active where userid=@PKey;", Con);
                Con.Open();
                int active = 0;
                if (checkActive.Checked == true)
                {
                    active = 1;
                }
                cmd.Parameters.AddWithValue("@userid", txtUserID.Text);
                cmd.Parameters.AddWithValue("@email", txtEmail.Text);
                cmd.Parameters.AddWithValue("@username", txtUsername.Text);
                cmd.Parameters.AddWithValue("@gender", cboGender.Text);
                cmd.Parameters.AddWithValue("@password", txtPassword.Text);
                cmd.Parameters.AddWithValue("@address", txtAddress.Text);
                cmd.Parameters.AddWithValue("@date_of_birth", dateTimePicker1.Text);
                cmd.Parameters.AddWithValue("@phone", txtPhone.Text);
                cmd.Parameters.AddWithValue("@active", active);
                cmd.Parameters.AddWithValue("@PKey", Key);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                MessageBox.Show("User Updated!");
                Refresh();
                Con.Close();
            }
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            new AddUser(dgv1).Show();
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void dgv1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
