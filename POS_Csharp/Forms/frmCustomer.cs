﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace POS_Csharp.Forms
{
    public partial class frmCustomer : Form
    {
        public frmCustomer()
        {
            InitializeComponent();
            DisplayCus();
            Refresh();
            Count_rows();
        }

        SqlConnection Con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");

        private void DisplayCus()
        {
            Con.Open();
            String q = ("SELECT cusid, cusname, address, phone,active FROM poscust");
            SqlCommand sqli = new SqlCommand(q, Con);
            SqlDataAdapter sql = new SqlDataAdapter(q, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            while (reader.Read())
            {
                dgvCus.Rows.Add(reader["cusid"], reader["cusname"], reader["phone"], reader["address"], reader["active"]);
                dgvCus.RowHeadersVisible = false;
                //count = dgvCus.RowCount;
                //lblCount.Text = count.ToString() + " records";
            }
            Con.Close();
            
        }
        public int Count_rows()
        {
            int count = 0;
            Con.Open();
            string stmt = "SELECT COUNT(*) FROM dbo.poscust";
            SqlCommand cmdCount = new SqlCommand(stmt, Con);
            count = (int)cmdCount.ExecuteScalar();
            Con.Close();
            lblCount.Text = count.ToString() + " records";
            return count;
        }
        private void Refresh()
        {
            Con.Open();
            String q = ("SELECT cusid, cusname, address, phone,active FROM poscust WHERE cusid like'" + this.txtSearch.Text + "%' OR cusname like'" + this.txtSearch.Text + "%'");
            SqlCommand sqli = new SqlCommand(q, Con);
            SqlDataAdapter sql = new SqlDataAdapter(q, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            dgvCus.Rows.Clear();
            while (reader.Read())
            {
                dgvCus.Rows.Add(reader["cusid"], reader["cusname"], reader["phone"], reader["address"], reader["active"]);
                dgvCus.RowHeadersVisible = false;
            }
            Con.Close();
        }

        private void Reset()
        {
            txtCustomerID.Text = "";
            txtCustomerName.Text = "";
            txtAddress.Text = "";
            txtPhone.Text = "";
            checkActive.Checked = false;
        }

        int Key = 0;
        private void dgvCus_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                txtCustomerID.Text = dgvCus.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtCustomerName.Text = dgvCus.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtAddress.Text = dgvCus.Rows[e.RowIndex].Cells[3].Value.ToString();
                txtPhone.Text = dgvCus.Rows[e.RowIndex].Cells[2].Value.ToString();
                if (txtCustomerID.Text == "")
                {
                    Key = 0;
                }
                else
                {
                    Key = Convert.ToInt32(dgvCus.Rows[e.RowIndex].Cells[0].Value.ToString());
                }

                if (dgvCus.Rows[e.RowIndex].Cells[4].Value.ToString() == "1")
                {
                    checkActive.Checked = true;
                }
                else
                {
                    checkActive.Checked = false;
                }
            }
            else
            {
                Reset();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("update poscust set cusid=@cusid, cusname=@cusname, phone=@phone, address=@address, active=@active where cusid=@PKey;", Con);
            Con.Open();
            int active = 0;
            if (checkActive.Checked == true)
            {
                active = 1;
            }
            cmd.Parameters.AddWithValue("@cusid", txtCustomerID.Text);
            cmd.Parameters.AddWithValue("@cusname", txtCustomerName.Text);
            cmd.Parameters.AddWithValue("@phone", txtPhone.Text);
            cmd.Parameters.AddWithValue("@address", txtAddress.Text);
            cmd.Parameters.AddWithValue("@active", active);
            cmd.Parameters.AddWithValue("@PKey", Key);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Con.Close();
            MessageBox.Show("Customer Updated!");
            Refresh();
            
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtCustomerID.Text == "")
            {
                MessageBox.Show("Please select customer you want to delete!");
            }
            else
            {
                if (MessageBox.Show("Do you want to delete this customer?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        Con.Open();
                        SqlCommand cmd = new SqlCommand("delete FROM poscust where cusname = @cusname", Con);
                        cmd.Parameters.AddWithValue("@cusname", txtCustomerName.Text);

                        cmd.ExecuteNonQuery();
                        Con.Close();
                        MessageBox.Show("Customer Deleted!");
                        Refresh();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                Reset();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new frmAddCus(dgvCus).Show();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            Refresh();
        }
    }
}
