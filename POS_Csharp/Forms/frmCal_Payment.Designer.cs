﻿namespace POS_Csharp.Forms
{
    partial class frmCal_Payment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCal_Payment));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new FontAwesome.Sharp.IconButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCancel = new FontAwesome.Sharp.IconButton();
            this.btnInvoice = new FontAwesome.Sharp.IconButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCreated_by = new System.Windows.Forms.TextBox();
            this.Date = new System.Windows.Forms.DateTimePicker();
            this.txtRiel = new System.Windows.Forms.TextBox();
            this.txtChange_R = new System.Windows.Forms.TextBox();
            this.txtRe_Riel = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtChange_rate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtChange_D = new System.Windows.Forms.TextBox();
            this.txtDollar = new System.Windows.Forms.TextBox();
            this.lblCusType = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblIn_No = new System.Windows.Forms.Label();
            this.lblInvNo = new System.Windows.Forms.Label();
            this.txtRe_Dollar = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Blue;
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(606, 46);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnClose.IconColor = System.Drawing.Color.Black;
            this.btnClose.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnClose.Location = new System.Drawing.Point(575, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(30, 25);
            this.btnClose.TabIndex = 1;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(249, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Payment";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.btnCancel);
            this.panel2.Controls.Add(this.btnInvoice);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 266);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(606, 56);
            this.panel2.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Maroon;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnCancel.FlatAppearance.BorderSize = 2;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnCancel.IconColor = System.Drawing.Color.Black;
            this.btnCancel.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnCancel.Location = new System.Drawing.Point(3, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(299, 50);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Back";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnInvoice
            // 
            this.btnInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInvoice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnInvoice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnInvoice.FlatAppearance.BorderSize = 2;
            this.btnInvoice.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Navy;
            this.btnInvoice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInvoice.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnInvoice.ForeColor = System.Drawing.Color.White;
            this.btnInvoice.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnInvoice.IconColor = System.Drawing.Color.Black;
            this.btnInvoice.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnInvoice.Location = new System.Drawing.Point(303, 3);
            this.btnInvoice.Name = "btnInvoice";
            this.btnInvoice.Size = new System.Drawing.Size(300, 50);
            this.btnInvoice.TabIndex = 0;
            this.btnInvoice.Text = "Invoice";
            this.btnInvoice.UseVisualStyleBackColor = false;
            this.btnInvoice.Click += new System.EventHandler(this.btnInvoice_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 46);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(606, 220);
            this.panel3.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.txtCreated_by);
            this.panel6.Controls.Add(this.Date);
            this.panel6.Controls.Add(this.txtRiel);
            this.panel6.Controls.Add(this.txtChange_R);
            this.panel6.Controls.Add(this.txtRe_Riel);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(373, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(233, 220);
            this.panel6.TabIndex = 1;
            this.panel6.Paint += new System.Windows.Forms.PaintEventHandler(this.panel6_Paint);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(25, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 23);
            this.label7.TabIndex = 18;
            this.label7.Text = "By:";
            // 
            // txtCreated_by
            // 
            this.txtCreated_by.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCreated_by.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtCreated_by.Location = new System.Drawing.Point(66, 10);
            this.txtCreated_by.Name = "txtCreated_by";
            this.txtCreated_by.Size = new System.Drawing.Size(97, 22);
            this.txtCreated_by.TabIndex = 17;
            this.txtCreated_by.Text = "Rady";
            this.txtCreated_by.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Date
            // 
            this.Date.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Date.Location = new System.Drawing.Point(54, 184);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(109, 22);
            this.Date.TabIndex = 2;
            this.Date.Visible = false;
            // 
            // txtRiel
            // 
            this.txtRiel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtRiel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRiel.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtRiel.Location = new System.Drawing.Point(15, 47);
            this.txtRiel.Name = "txtRiel";
            this.txtRiel.ReadOnly = true;
            this.txtRiel.Size = new System.Drawing.Size(165, 25);
            this.txtRiel.TabIndex = 16;
            this.txtRiel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtChange_R
            // 
            this.txtChange_R.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtChange_R.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChange_R.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtChange_R.Location = new System.Drawing.Point(15, 129);
            this.txtChange_R.Name = "txtChange_R";
            this.txtChange_R.ReadOnly = true;
            this.txtChange_R.Size = new System.Drawing.Size(165, 25);
            this.txtChange_R.TabIndex = 15;
            this.txtChange_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtRe_Riel
            // 
            this.txtRe_Riel.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtRe_Riel.Location = new System.Drawing.Point(15, 84);
            this.txtRe_Riel.Name = "txtRe_Riel";
            this.txtRe_Riel.PlaceholderText = "Riel";
            this.txtRe_Riel.Size = new System.Drawing.Size(165, 32);
            this.txtRe_Riel.TabIndex = 8;
            this.txtRe_Riel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRe_Riel.TextChanged += new System.EventHandler(this.txtRe_Riel_TextChanged);
            this.txtRe_Riel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRe_Riel_KeyPress);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.textBox1);
            this.panel4.Controls.Add(this.txtChange_rate);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.txtChange_D);
            this.panel4.Controls.Add(this.txtDollar);
            this.panel4.Controls.Add(this.lblCusType);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.lblIn_No);
            this.panel4.Controls.Add(this.lblInvNo);
            this.panel4.Controls.Add(this.txtRe_Dollar);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(367, 220);
            this.panel4.TabIndex = 0;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.textBox1.Location = new System.Drawing.Point(134, 184);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(49, 22);
            this.textBox1.TabIndex = 17;
            this.textBox1.Text = "1$  =";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtChange_rate
            // 
            this.txtChange_rate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtChange_rate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtChange_rate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChange_rate.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtChange_rate.Location = new System.Drawing.Point(189, 184);
            this.txtChange_rate.Name = "txtChange_rate";
            this.txtChange_rate.ReadOnly = true;
            this.txtChange_rate.Size = new System.Drawing.Size(96, 22);
            this.txtChange_rate.TabIndex = 16;
            this.txtChange_rate.Text = "4000";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(12, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 23);
            this.label6.TabIndex = 15;
            this.label6.Text = "Exchange rate:";
            // 
            // txtChange_D
            // 
            this.txtChange_D.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtChange_D.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChange_D.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtChange_D.Location = new System.Drawing.Point(150, 129);
            this.txtChange_D.Name = "txtChange_D";
            this.txtChange_D.ReadOnly = true;
            this.txtChange_D.Size = new System.Drawing.Size(165, 25);
            this.txtChange_D.TabIndex = 14;
            this.txtChange_D.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDollar
            // 
            this.txtDollar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtDollar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDollar.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtDollar.Location = new System.Drawing.Point(150, 47);
            this.txtDollar.Name = "txtDollar";
            this.txtDollar.ReadOnly = true;
            this.txtDollar.Size = new System.Drawing.Size(165, 25);
            this.txtDollar.TabIndex = 13;
            this.txtDollar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDollar.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblCusType
            // 
            this.lblCusType.AutoSize = true;
            this.lblCusType.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblCusType.ForeColor = System.Drawing.Color.Black;
            this.lblCusType.Location = new System.Drawing.Point(204, 5);
            this.lblCusType.Name = "lblCusType";
            this.lblCusType.Size = new System.Drawing.Size(78, 23);
            this.lblCusType.TabIndex = 12;
            this.lblCusType.Text = "CusType";
            this.lblCusType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(153, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 23);
            this.label4.TabIndex = 11;
            this.label4.Text = "Cus:";
            // 
            // lblIn_No
            // 
            this.lblIn_No.AutoSize = true;
            this.lblIn_No.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblIn_No.ForeColor = System.Drawing.Color.Black;
            this.lblIn_No.Location = new System.Drawing.Point(46, 5);
            this.lblIn_No.Name = "lblIn_No";
            this.lblIn_No.Size = new System.Drawing.Size(65, 23);
            this.lblIn_No.TabIndex = 10;
            this.lblIn_No.Text = "Invoice";
            // 
            // lblInvNo
            // 
            this.lblInvNo.AutoSize = true;
            this.lblInvNo.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblInvNo.Location = new System.Drawing.Point(12, 5);
            this.lblInvNo.Name = "lblInvNo";
            this.lblInvNo.Size = new System.Drawing.Size(36, 23);
            this.lblInvNo.TabIndex = 9;
            this.lblInvNo.Text = "No:";
            // 
            // txtRe_Dollar
            // 
            this.txtRe_Dollar.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtRe_Dollar.Location = new System.Drawing.Point(150, 84);
            this.txtRe_Dollar.Name = "txtRe_Dollar";
            this.txtRe_Dollar.PlaceholderText = "$";
            this.txtRe_Dollar.Size = new System.Drawing.Size(165, 32);
            this.txtRe_Dollar.TabIndex = 7;
            this.txtRe_Dollar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRe_Dollar.TextChanged += new System.EventHandler(this.txtRe_Dollar_TextChanged);
            this.txtRe_Dollar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRe_Dollar_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(12, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 23);
            this.label5.TabIndex = 6;
            this.label5.Text = "Change";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(12, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Received";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(9, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Total Amount";
            // 
            // frmCal_Payment
            // 
            this.AcceptButton = this.btnInvoice;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(606, 322);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmCal_Payment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cal_Payment";
            this.Load += new System.EventHandler(this.frmCal_Payment_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private Panel panel2;
        private Panel panel3;
        private Label label1;
        private FontAwesome.Sharp.IconButton btnClose;
        private Panel panel4;
        private Panel panel6;
        private Label label2;
        private Label label5;
        private Label label3;
        private TextBox txtRe_Dollar;
        private TextBox txtRe_Riel;
        private FontAwesome.Sharp.IconButton btnCancel;
        private FontAwesome.Sharp.IconButton btnInvoice;
        private Label lblInvNo;
        private Label lblIn_No;
        private Label label4;
        private Label lblCusType;
        private TextBox txtChange_D;
        private TextBox txtDollar;
        private TextBox txtChange_R;
        private TextBox txtRiel;
        private Label label6;
        private TextBox txtChange_rate;
        private TextBox textBox1;
        private DateTimePicker Date;
        private TextBox txtCreated_by;
        private Label label7;
    }
}