﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

namespace POS_Csharp.Forms
{
    public partial class frmCal_Payment : Form
    {
        public static frmCal_Payment instance;
        public TextBox txtTotalAmountD;
        public TextBox txtTotalAmountR;
        public Label lblInvoice;
        public Label lblCusID_Type;
        public Label productid;
        public Label produdtname;
        public Label qty;
        public Label price;
        public Label sumid;
        public Label lblCount; 
        public Label lblSub;
        public Label lblDis_Amount;
        public Label lblInv_No;
        public Label lblTotal;
        private DataGridView dataGridView1;

        public frmCal_Payment(DataGridView dataGridView1, Label lblCount, Label lblSub, Label lblDis_Amount, Label lblTotal)
        {
            InitializeComponent();
            this.dataGridView1 = dataGridView1;
            instance = this;
            txtTotalAmountD = txtDollar;
            txtTotalAmountR = txtRiel;
            lblInvoice = lblIn_No;
            lblCusID_Type = lblCusType;
            btnInvoice.Enabled = false;
            this.lblTotal = lblTotal;
            this.lblCount = lblCount;
            this.lblDis_Amount = lblDis_Amount;
            this.lblSub = lblSub;
            refresh();
        }
        SqlConnection con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");
            

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);

        private void refresh()
        {
            invoiceID();
            txtTotalAmountD.Clear();

        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            refresh();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        protected override void WndProc(ref Message m)
        {
            const int WM_NCCALCSIZE = 0x0083;
            if (m.Msg == WM_NCCALCSIZE && m.WParam.ToInt32() == 1)
            {
                return;
            }
            base.WndProc(ref m);
        }

        private void frmCal_Payment_Load(object sender, EventArgs e)
        {
            btnInvoice.Enabled = false;
            //MessageBox.Show(lblIn_No.ToString());
            productid = new Label();
            productid.Text = productid.ToString();
            productid.Hide();
            produdtname = new Label();
            produdtname.Text = produdtname.ToString();
            produdtname.Hide();
            qty = new Label();
            qty.Text = 1 + qty.ToString();
            qty.Hide();
            price = new Label();
            price.Text = price.ToString();
            price.Hide();
            sumid = new Label();
            sumid.Text = sumid.ToString();
            sumid.Hide();
        }

        private void lblDollar_Click(object sender, EventArgs e)
        {
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblRiel_Click(object sender, EventArgs e)
        {

        }

        private void txtRe_Dollar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double a, b, c;
                a = Convert.ToDouble(txtRe_Dollar.Text);
                b = Convert.ToDouble(txtDollar.Text.Replace("$", ""));
                c = a - b;

                if(c < 0)
                {
                    btnInvoice.Enabled = false;
                    txtChange_D.Text = "Not enough money!";
                    txtChange_D.ForeColor = Color.Red;
                }
                else
                {
                    if (c == 0)
                    {
                        btnInvoice.Enabled = true;
                        txtChange_D.Text = c.ToString("$#,##0.00#,##");
                        txtChange_D.ForeColor = Color.Black;
                    }
                }
            }
            catch(Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            calculate();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            float ex_rate = 4000;
            string input1 = txtRe_Dollar.Text.Trim();
            string input2 = txtRe_Riel.Text.Trim();
            double.TryParse(input1, out double input_usd);
            double.TryParse(input2, out double input_riel);

            double user_cash = input_usd + input_riel / ex_rate;

            double amount = double.Parse(txtDollar.Text.ToString().Replace("$", ""));
            double change_usd = user_cash - amount;
            double riel = change_usd * ex_rate;
            double toriel = amount * ex_rate;
            txtRiel.Text = toriel.ToString("#,##0,###Riel").Replace("Riel", "")+ "Riel";
        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtRe_Riel_TextChanged(object sender, EventArgs e)
        {
            calculate();
        }
        public void invoiceID()
        {
            con.Open();
            string i = "No-";
            int n = 10000;
            string query = "select top 1 invid from posheader order by invid desc";
            SqlCommand cc = new SqlCommand(query, con);
            SqlDataReader dr = cc.ExecuteReader();
            if (!dr.HasRows)
            {
                lblInvoice.Text = "No-" + n.ToString();
            }
            else
            {
                dr.Read();
                string ss = dr["invid"].ToString();
                string[] aa = ss.Split("-");
                n = int.Parse(aa[1]) + 1;
                lblInvoice.Text = i + n.ToString();
                //MessageBox.Show("in: "+lblInvoice.Text);
            }
            dr.Close();
            con.Close();
        }
        public void calculate()
        {
            float ex_rate = 4000;
            string input1 = txtRe_Dollar.Text.Trim();
            string input2 = txtRe_Riel.Text.Trim();
            double.TryParse(input1, out double input_usd);
            double.TryParse(input2, out double input_riel);

            double user_cash = input_usd + input_riel / ex_rate;

            double amount = double.Parse(txtDollar.Text.ToString().Replace("$", ""));
            double change_usd = user_cash - amount;
            double riel = change_usd * ex_rate;
            double toriel = amount * ex_rate;
            txtRiel.Text = toriel.ToString("#,##0.###Riel").Replace("Riel", "")+ "Riel";
            if (change_usd >= 0)
            {
                btnInvoice.Enabled = true;

                txtChange_D.Text = change_usd.ToString("$#,##0.00#,##");
                txtChange_R.Text = riel.ToString("#,##0.###Riel");
                txtChange_D.ForeColor = Color.Black;
                txtChange_R.ForeColor = Color.Black;
                if (change_usd > 0)
                {
                    txtChange_D.Text = change_usd.ToString("$#,##0.00#,##");
                    txtChange_R.Text = riel.ToString("#,##0,###Riel");
                    txtChange_D.ForeColor = Color.Black;
                    txtChange_R.ForeColor = Color.Black;
                }
            }
            else
            {
                btnInvoice.Enabled = false;
                txtChange_D.Text = change_usd.ToString("$#,##0.00#,##");
                txtChange_R.Text = riel.ToString("#,##0,###Riel");
                txtChange_D.ForeColor = Color.Red;
                txtChange_R.ForeColor = Color.Red;

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnInvoice_Click(object sender, EventArgs e)
        {
            get_Invoice();
            Add_detial_header();
            add_setting();
        }
        private void Add_detial_header()
        {
            if (dataGridView1.Rows.Count > 0)
            {
                con.Open();
                try
                {
                    string sql = "insert into posheader(invid, posdate, custid, custname, amount, paid_amount, created_by, created_at) " +
                                             $"values ('{lblIn_No.Text}','{Date.Value}','{lblCusType.Text}','{lblCusType.Text}',{txtDollar.Text},{txtDollar.Text},'{txtCreated_by.Text}','{Date.Value}');" + Environment.NewLine;

                    foreach (DataGridViewRow row in dataGridView1.Rows)
                    {
                        sql += "insert into posdetail(invid, posdate, custid, productid, productname, qty, price, total_amount, sumid, created_by, created_at) " +
                                              $"values ('{lblIn_No.Text}','{Date.Value}','{lblCusType.Text}','{row.Cells[1].Value.ToString()}','{row.Cells[0].Value.ToString()}',{row.Cells[3].Value.ToString()},{row.Cells[5].Value.ToString()},{txtDollar.Text},'{row.Cells[2].Value.ToString()}','{txtCreated_by.Text}','{Date.Value}');";

                    }
                    //MessageBox.Show(sql);
                    SqlCommand detail = new SqlCommand(sql, con);
                    detail.ExecuteNonQuery();
                    //MessageBox.Show("Successs!");
                    con.Close();
                    //this.Close();
                    lblCount.Text = "(0)";
                    lblSub.Text = "$0.00";
                    lblDis_Amount.Text = "$0.00";
                    lblTotal.Text = "$0.00";
                    dataGridView1.Rows.Clear();
                    invoiceID();
                }
                    catch (Exception ex)
                {
                    MessageBox.Show($"Error > {ex.Message}");
                }
            }

        }
        private void add_setting()
        {
            con.Open();
            string i = "No-";
            int n = 10000;
            string query = ("SELECT TOP 1 invid FROM posheader ORDER BY invid DESC");
            SqlCommand sql = new SqlCommand(query, con);
            SqlDataReader reader = sql.ExecuteReader();
            //if (!reader.HasRows)
            //{
            //    lblInvoice.Text = "No-" + n.ToString();
            //}
            //else
            //{
                if (reader.Read())
                {
                    string ss = reader["invid"].ToString();
                    string[] aa = ss.Split("-");
                    n = int.Parse(aa[1]);
                    reader.Close();
                    //lblInvoice.Text = i + n.ToString();
                    string qq = "Delete from setting;";
                    qq += "insert into setting(invoice, invoiceformat,created_at) " +
                                                 $"values ('{n}','{i}','{Date.Value}');";

                    SqlCommand cm = new SqlCommand(qq, con);
                    cm.ExecuteNonQuery();
                }
            //}
            con.Close();
            if (con.State == ConnectionState.Open)
                con.Close();
        }
        private void get_Invoice()
        {
            frmInvoice formInc = new frmInvoice(dataGridView1);
            this.Hide();
            //formInc.Show();

            frmInvoice.instance.Invoice.Text = lblIn_No.Text;
            frmInvoice.instance.Date_Inv.Text = Date.Text;
            frmInvoice.instance.Customer.Text = lblCusType.Text;
            frmInvoice.instance.Cashier.Text = txtCreated_by.Text;
            frmInvoice.instance.Received_Dollar.Text = txtRe_Dollar.Text;
            frmInvoice.instance.Received_Riel.Text = txtRe_Riel.Text;
            frmInvoice.instance.Subtotal_Dollar.Text = lblSub.Text;
            frmInvoice.instance.Subtotal_Riel.Text = lblSub.Text;
            frmInvoice.instance.TotalDiscount_Dollar.Text = lblDis_Amount.Text;
            frmInvoice.instance.TotalDiscount_Riel.Text = lblDis_Amount.Text;
            frmInvoice.instance.TotalAmount_Dollar.Text = txtDollar.Text;
            frmInvoice.instance.TotalAmount_Riel.Text = txtRiel.Text;
            frmInvoice.instance.Change_Dollar.Text = txtChange_D.Text;
            frmInvoice.instance.Change_Riel.Text = txtChange_R.Text;
            formInc.ShowDialog();
        }
        private void txtRe_Dollar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
            (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtRe_Riel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
            (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
