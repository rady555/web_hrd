﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace POS_Csharp.Forms
{
    public partial class Dashboard : Form
    {

        public Dashboard()
        {
            InitializeComponent();
            select_Pro();
            select_Cate();
            select_Sale();
            select_Cus();
            select_User();
        }

        SqlConnection Con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");

        private void Dashboard_Load(object sender, EventArgs e)
        {
            btn_Size();
            select_Pro();
        }
        private void btn_Size()
        {
            //if(this.panel1.Width <= 1010)
            //{
            //    btnsale.Width = 196;
            //}
            //else
            //{
            //    btnsale.Width = 250;
            //}
        }
        public int select_Pro()
        {
            int count = 0;
            Con.Open();
            string stmt = "SELECT COUNT(*) FROM dbo.icproduct;";
            SqlCommand cmdCount = new SqlCommand(stmt, Con);
            count = (int)cmdCount.ExecuteScalar();
            Con.Close();
            txtpro.Text = count.ToString();
            return count;
        }
        public int select_Cate()
        {
            int count = 0;
            Con.Open();
            string stmt = "SELECT COUNT(*) FROM dbo.iccategory;";
            SqlCommand cmdCount = new SqlCommand(stmt, Con);
            count = (int)cmdCount.ExecuteScalar();
            Con.Close();
            txtcate.Text = count.ToString();
            return count;
        }
        public int select_Sale()
        {
            int count = 0;
            Con.Open();
            string stmt = "SELECT COUNT(*) FROM dbo.posheader;";
            SqlCommand cmdCount = new SqlCommand(stmt, Con);
            count = (int)cmdCount.ExecuteScalar();
            Con.Close();
            txtSale.Text = count.ToString();
            return count;
        }
        public int select_Cus()
        {
            int count = 0;
            Con.Open();
            string stmt = "SELECT COUNT(*) FROM dbo.poscust;";
            SqlCommand cmdCount = new SqlCommand(stmt, Con);
            count = (int)cmdCount.ExecuteScalar();
            Con.Close();
            txtCus.Text = count.ToString();
            return count;
        }
        public int select_User()
        {
            int count = 0;
            Con.Open();
            string stmt = "SELECT COUNT(*) FROM dbo.sysuser;";
            SqlCommand cmdCount = new SqlCommand(stmt, Con);
            count = (int)cmdCount.ExecuteScalar();
            Con.Close();
            txtUser.Text = count.ToString();
            return count;
        }
    }
}
