﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

namespace POS_Csharp.Forms
{
    public partial class AddUser : Form
    {
        private DataGridView dgv1 = null;
        public AddUser(DataGridView dgv1)
        {
            InitializeComponent();
            this.dgv1 = dgv1;
            Userid();
            Refresh();
        }

        SqlConnection con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);


        private void btnBack_Click(object sender, EventArgs e)
        {
            //new frmUser().Show();
            this.Close();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        protected override void WndProc(ref Message m)
        {
            const int WM_NCCALCSIZE = 0x0083;
            if (m.Msg == WM_NCCALCSIZE && m.WParam.ToInt32() == 1)
            {
                return;
            }
            base.WndProc(ref m);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtEmail1.Clear();
            txtUsername1.Clear();
            txtPassword1.Clear();
            txtPhone1.Clear();
            txtAddress1.Clear();
            checkActive1.Checked = false;
        }
        private void Reset()
        {
            // txtUserID.Text = "";
            txtUsername1.Text = "";
            txtEmail1.Text = "";
            txtPhone1.Text = "";
            txtPassword1.Text = "";
            cboGender1.Text = "";
            txtAddress1.Text = "";
            checkActive1.Text = "";
            checkActive1.Checked = false;

        }

        private void Refresh()
        {
            con.Open();
            String q = ("SELECT userid, email, username, gender, password, address, active, phone, date_of_birth FROM sysuser");
            SqlCommand sqli = new SqlCommand(q, con);
            SqlDataAdapter sql = new SqlDataAdapter(q, con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            dgv1.Rows.Clear();
            while (reader.Read())
            {
                dgv1.Rows.Add(reader["userid"], reader["email"], reader["username"], reader["gender"], reader["password"], reader["address"], reader["active"], reader["phone"], reader["date_of_birth"]); ;
                dgv1.RowHeadersVisible = false;
            }
            Reset();
            con.Close();
        }

        public void Userid()
        {
            con.Open();
            int i = 0;
            string query = "select top 1 userid from sysuser order by userid desc";
            SqlCommand cc = new SqlCommand(query, con);
            SqlDataReader dr = cc.ExecuteReader();
            while (dr.Read())
            {
                i = int.Parse(dr["userid"].ToString());
            }
            ++i;
            txtUserID1.Text = i.ToString();
            dr.Close();
            con.Close();
        }
        int key = 0;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtUsername1.Text == "")
            {
                key = 0;
                MessageBox.Show("Please input email!");
            }
            else if (txtEmail1.Text == "")
            {
                key = 0;
                MessageBox.Show("Please input username!");
            }
            else if (txtPassword1.Text == "")
            {
                key = 0;
                MessageBox.Show("Please input password!");
            }
            else if (txtPhone1.Text == "")
            {
                key = 0;
                MessageBox.Show("Please input phone number!");
            }
            else if (txtAddress1.Text == "")
            {
                key = 0;
                MessageBox.Show("Please input address!");
            }
            else if (cboGender1.Text == "")
            {
                key = 0;
                MessageBox.Show("Please input select your gender!");
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");
                con.Open();
                SqlCommand cmd = new SqlCommand("insert into sysuser(userid, email, username, gender, password, address, date_of_birth, phone, active) values (@userid,@email,@username,@gender,@password,@address,@date_of_birth,@phone, @active)", con);
                int active = 0;
                if (checkActive1.Checked == true)
                {
                    active = 1;
                }
                cmd.Parameters.AddWithValue("@userid", txtUserID1.Text);
                cmd.Parameters.AddWithValue("@email", txtEmail1.Text);
                cmd.Parameters.AddWithValue("@username", txtUsername1.Text);
                cmd.Parameters.AddWithValue("@gender", cboGender1.Text);
                cmd.Parameters.AddWithValue("@password", txtPassword1.Text);
                cmd.Parameters.AddWithValue("@address", txtAddress1.Text);
                //MessageBox.Show(dateTimePicker1.Value.ToString());
                cmd.Parameters.AddWithValue("@date_of_birth", (dateTimePicker.Value));
                cmd.Parameters.AddWithValue("@phone", txtPhone1.Text);
                cmd.Parameters.AddWithValue("@active", active);

                try
                {
                    cmd.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error > {ex.Message}");
                }
                //Userid();
                //Reset();
                if (MessageBox.Show("Do you want to add more user?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Userid();
                    Reset();
                    Refresh();
                }
                else
                {
                    this.Close();
                    Refresh();
                    con.Close();
                }
                    //MessageBox.Show("User Added!");

               /* con.Open();
                String q = ("SELECT userid, email, username, gender, password, address, active, phone, date_of_birth FROM sysuser");
                SqlCommand sqli = new SqlCommand(q, con);
                SqlDataAdapter sql = new SqlDataAdapter(q, con);
                SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
                var ds = new DataSet();
                
                var reader = sqli.ExecuteReader();

                dgv1.Rows.Clear();
                while (reader.Read())
                {
                    dgv1.Rows.Add(reader["userid"], reader["email"], reader["username"], reader["gender"], reader["password"], reader["address"], reader["active"], reader["phone"], reader["date_of_birth"]); ;
                    dgv1.RowHeadersVisible = false;
                }
                Reset();
                con.Close();*/
            }
        }

    }
}
