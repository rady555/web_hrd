﻿namespace POS_Csharp.Forms
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.txtCus = new System.Windows.Forms.TextBox();
            this.txtSale = new System.Windows.Forms.TextBox();
            this.txtpro = new System.Windows.Forms.TextBox();
            this.txtcate = new System.Windows.Forms.TextBox();
            this.btnuser = new FontAwesome.Sharp.IconButton();
            this.btncus = new FontAwesome.Sharp.IconButton();
            this.btncate = new FontAwesome.Sharp.IconButton();
            this.btnpro = new FontAwesome.Sharp.IconButton();
            this.btnsale = new FontAwesome.Sharp.IconButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.txtUser);
            this.panel1.Controls.Add(this.txtCus);
            this.panel1.Controls.Add(this.txtSale);
            this.panel1.Controls.Add(this.txtpro);
            this.panel1.Controls.Add(this.txtcate);
            this.panel1.Controls.Add(this.btnuser);
            this.panel1.Controls.Add(this.btncus);
            this.panel1.Controls.Add(this.btncate);
            this.panel1.Controls.Add(this.btnpro);
            this.panel1.Controls.Add(this.btnsale);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(994, 174);
            this.panel1.TabIndex = 0;
            // 
            // txtUser
            // 
            this.txtUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUser.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtUser.ForeColor = System.Drawing.Color.Maroon;
            this.txtUser.Location = new System.Drawing.Point(795, 80);
            this.txtUser.Name = "txtUser";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(184, 25);
            this.txtUser.TabIndex = 12;
            this.txtUser.Text = "0";
            this.txtUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCus
            // 
            this.txtCus.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtCus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCus.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtCus.ForeColor = System.Drawing.Color.White;
            this.txtCus.Location = new System.Drawing.Point(599, 80);
            this.txtCus.Name = "txtCus";
            this.txtCus.ReadOnly = true;
            this.txtCus.Size = new System.Drawing.Size(184, 25);
            this.txtCus.TabIndex = 11;
            this.txtCus.Text = "0";
            this.txtCus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSale
            // 
            this.txtSale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.txtSale.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSale.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtSale.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtSale.Location = new System.Drawing.Point(10, 81);
            this.txtSale.Name = "txtSale";
            this.txtSale.ReadOnly = true;
            this.txtSale.Size = new System.Drawing.Size(184, 25);
            this.txtSale.TabIndex = 10;
            this.txtSale.Text = "0";
            this.txtSale.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtpro
            // 
            this.txtpro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.txtpro.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtpro.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtpro.ForeColor = System.Drawing.Color.Indigo;
            this.txtpro.Location = new System.Drawing.Point(207, 81);
            this.txtpro.Name = "txtpro";
            this.txtpro.ReadOnly = true;
            this.txtpro.Size = new System.Drawing.Size(184, 25);
            this.txtpro.TabIndex = 9;
            this.txtpro.Text = "0";
            this.txtpro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtcate
            // 
            this.txtcate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtcate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcate.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtcate.Location = new System.Drawing.Point(403, 80);
            this.txtcate.Name = "txtcate";
            this.txtcate.ReadOnly = true;
            this.txtcate.Size = new System.Drawing.Size(184, 25);
            this.txtcate.TabIndex = 7;
            this.txtcate.Text = "0";
            this.txtcate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnuser
            // 
            this.btnuser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnuser.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnuser.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnuser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnuser.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnuser.ForeColor = System.Drawing.Color.Maroon;
            this.btnuser.IconChar = FontAwesome.Sharp.IconChar.UserFriends;
            this.btnuser.IconColor = System.Drawing.Color.Maroon;
            this.btnuser.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnuser.IconSize = 80;
            this.btnuser.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnuser.Location = new System.Drawing.Point(789, 5);
            this.btnuser.Name = "btnuser";
            this.btnuser.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.btnuser.Size = new System.Drawing.Size(196, 160);
            this.btnuser.TabIndex = 4;
            this.btnuser.Text = "User";
            this.btnuser.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnuser.UseVisualStyleBackColor = false;
            // 
            // btncus
            // 
            this.btncus.BackColor = System.Drawing.SystemColors.Highlight;
            this.btncus.Dock = System.Windows.Forms.DockStyle.Left;
            this.btncus.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btncus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncus.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btncus.ForeColor = System.Drawing.Color.White;
            this.btncus.IconChar = FontAwesome.Sharp.IconChar.PeopleCarry;
            this.btncus.IconColor = System.Drawing.Color.White;
            this.btncus.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btncus.IconSize = 80;
            this.btncus.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btncus.Location = new System.Drawing.Point(593, 5);
            this.btncus.Name = "btncus";
            this.btncus.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.btncus.Size = new System.Drawing.Size(196, 160);
            this.btncus.TabIndex = 3;
            this.btncus.Text = "Customer";
            this.btncus.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btncus.UseVisualStyleBackColor = false;
            // 
            // btncate
            // 
            this.btncate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btncate.Dock = System.Windows.Forms.DockStyle.Left;
            this.btncate.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btncate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncate.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btncate.IconChar = FontAwesome.Sharp.IconChar.ListUl;
            this.btncate.IconColor = System.Drawing.Color.Black;
            this.btncate.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btncate.IconSize = 80;
            this.btncate.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btncate.Location = new System.Drawing.Point(397, 5);
            this.btncate.Name = "btncate";
            this.btncate.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.btncate.Size = new System.Drawing.Size(196, 160);
            this.btncate.TabIndex = 2;
            this.btncate.Text = "Category";
            this.btncate.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btncate.UseVisualStyleBackColor = false;
            // 
            // btnpro
            // 
            this.btnpro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnpro.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnpro.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnpro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpro.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnpro.ForeColor = System.Drawing.Color.Indigo;
            this.btnpro.IconChar = FontAwesome.Sharp.IconChar.ProductHunt;
            this.btnpro.IconColor = System.Drawing.Color.Indigo;
            this.btnpro.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnpro.IconSize = 80;
            this.btnpro.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnpro.Location = new System.Drawing.Point(201, 5);
            this.btnpro.Name = "btnpro";
            this.btnpro.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.btnpro.Size = new System.Drawing.Size(196, 160);
            this.btnpro.TabIndex = 1;
            this.btnpro.Text = "Product";
            this.btnpro.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnpro.UseVisualStyleBackColor = false;
            // 
            // btnsale
            // 
            this.btnsale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnsale.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnsale.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnsale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsale.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnsale.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnsale.IconChar = FontAwesome.Sharp.IconChar.Sellsy;
            this.btnsale.IconColor = System.Drawing.SystemColors.HotTrack;
            this.btnsale.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnsale.IconSize = 80;
            this.btnsale.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnsale.Location = new System.Drawing.Point(5, 5);
            this.btnsale.Name = "btnsale";
            this.btnsale.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.btnsale.Size = new System.Drawing.Size(196, 160);
            this.btnsale.TabIndex = 0;
            this.btnsale.Text = "Sale";
            this.btnsale.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnsale.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 513);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(994, 118);
            this.panel2.TabIndex = 1;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(994, 631);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Dashboard";
            this.Text = "Dashboard";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private FontAwesome.Sharp.IconButton btnsale;
        private FontAwesome.Sharp.IconButton btncus;
        private FontAwesome.Sharp.IconButton btncate;
        private FontAwesome.Sharp.IconButton btnpro;
        private FontAwesome.Sharp.IconButton btnuser;
        private Panel panel2;
        private TextBox txtcate;
        private TextBox txtpro;
        private TextBox txtSale;
        private TextBox txtUser;
        private TextBox txtCus;
    }
}