﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace POS_Csharp.Forms
{
    public partial class frmProduct : Form
    {
        string ImageLocation = "";
        //SqlCommand cmd;
        private Image Default_img = null;
        public frmProduct()
        {
            InitializeComponent();
            DisplayPro();
            Refresh();
            Count_rows();
            Default_img = Image.FromFile(@"C:\Users\radyk\Downloads\no-image.png");
        }

        SqlConnection Con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");

        /* private void btnImage_Click(object sender, EventArgs e)
         {

             OpenFileDialog openFileDialog1 = new OpenFileDialog();
             openFileDialog1.Filter = "(*.BMP;*.JPG;*.GIF;*.JPEG;*.PNG)|*.BMP;*.JPG;*.GIF;*.JPEG;*.PNG";
             if (openFileDialog1.ShowDialog() == DialogResult.OK)
             {
                 imgloc = openFileDialog1.FileName.ToString();
                 imagePro.ImageLocation = imgloc;
             }
            dialog = new ();
            dialog.Filter = "Select  image(*.Jpg; *.png; *.Gif)|*.Jpg; *.png; *.Gif";
            if(dialog.ShowDialog()==DialogResult.OK)
            {
                //imagePro.Image = Image.FromFile(dialog.FileName);
                this.ImageLocation = dialog.FileName.ToString();
                MessageBox.Show(dialog.FileName.ToString());
                imagePro.ImageLocation = ImageLocation;
            }
        }*/

        private void imagePro_Click(object sender, EventArgs e)
        {

        }
        private void DisplayPro()
        {
            Con.Open();
            String q = ("SELECT TOP 20 productid,image, productname, categoryid,active, notes, price, umid, sumid FROM icproduct");
            SqlCommand sqli = new SqlCommand(q, Con);
            SqlDataAdapter sql = new SqlDataAdapter(q, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();       
            while (reader.Read())
            {
                byte[] bytes = null;
                Image image = null;
                if (DBNull.Value != reader["image"])
                {
                    bytes = (byte[])reader["image"];
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        image = Image.FromStream(ms);
                    }
                }
                
                dgvPro.Rows.Add(reader["productid"].ToString(), image, reader["productname"].ToString(), reader["price"].ToString(), reader["categoryid"].ToString(), reader["umid"].ToString(), reader["sumid"].ToString(), reader["notes"].ToString(), reader["active"].ToString());
                dgvPro.RowHeadersVisible = false;
                //int count = dgvPro.RowCount;
                //lblCount.Text = count.ToString() + " records";
            }
            Con.Close();
        }
        public int Count_rows()
        {
            int count = 0;
            Con.Open();
            string stmt = "SELECT COUNT(*) FROM dbo.icproduct";
            SqlCommand cmdCount = new SqlCommand(stmt, Con);
            count = (int)cmdCount.ExecuteScalar();
            Con.Close();
            lblCount.Text = count.ToString() + " records";
            return count;
        }

        private void Refresh()
        {
            Con.Open();
            String q = ("SELECT Top 20 productid,image, productname, categoryid,active, notes, price, umid, sumid FROM icproduct WHERE productid like'" + this.txtSearch.Text + "%' OR productname like'" + this.txtSearch.Text + "%'");
            SqlCommand sqli = new SqlCommand(q, Con);
            SqlDataAdapter sql = new SqlDataAdapter(q, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            dgvPro.Rows.Clear();         
            while (reader.Read())
            {
                byte[] bytes = null;
                Image image = null;
                if (DBNull.Value != reader["image"])
                {
                    bytes = (byte[])reader["image"];
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        image = Image.FromStream(ms);
                    }
                }
                if (image == null)
                {
                    image = Image.FromFile(@"C:\Users\radyk\Downloads\no-image.png");
                    //image = PictureBoxSizeMode.StretchImage;
                }
                dgvPro.Rows.Add(reader["productid"].ToString(), image, reader["productname"].ToString(), reader["price"].ToString(), reader["categoryid"].ToString(), reader["umid"].ToString(), reader["sumid"].ToString(), reader["notes"].ToString(), reader["active"].ToString());
                dgvPro.RowHeadersVisible = false;
            }
            Con.Close();
        }


        private void Reset()
        {
            txtProID.Text = "";
            imagePro.Image = null;
            txtProName.Text = "";
            txtPrice.Text = "";
            cboCateID.SelectedItem = "";
            cboumid.SelectedItem = "";
            cbosumid.SelectedItem = "";
            checkActive.Checked = false;
            txtNote.Text = "";
        }
        string Key = "0";
        private void dgvPro_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                txtProID.Text = dgvPro.Rows[e.RowIndex].Cells[0].Value.ToString();
                imagePro.Image = dgvPro.Rows[e.RowIndex].Cells[1].Value as Image;
                txtProName.Text = dgvPro.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtPrice.Text = dgvPro.Rows[e.RowIndex].Cells[3].Value.ToString();
                cboCateID.Text = dgvPro.Rows[e.RowIndex].Cells[4].Value.ToString();
                cboumid.Text = dgvPro.Rows[e.RowIndex].Cells[5].Value.ToString();
                cbosumid.Text = dgvPro.Rows[e.RowIndex].Cells[6].Value.ToString();
                txtNote.Text = dgvPro.Rows[e.RowIndex].Cells[7].Value.ToString();
                if (txtProID.Text == "")
                {
                    Key = "0";
                }
                else
                {
                    Key = dgvPro.Rows[e.RowIndex].Cells[0].Value.ToString();
                    //MessageBox.Show(Key);
                }
               
                if (dgvPro.Rows[e.RowIndex].Cells[8].Value.ToString() == "1")
                {
                    checkActive.Checked = true;
                }
                else
                {
                    checkActive.Checked = false;
                }
            }
            else
            {
                Reset();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Key == "0")
            {
                MessageBox.Show("Please select product!");
            }
            else
            {
                if (MessageBox.Show("Do you want to delete this product?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        Con.Open();
                        SqlCommand cmd = new SqlCommand("delete FROM icproduct where productid = @productid", Con);
                        cmd.Parameters.AddWithValue("@productid", txtProID.Text);

                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Product Deleted!");
                        Con.Close();
                        Refresh();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }
      
        private void btnEdit_Click(object sender, EventArgs e) 
        {
            SqlCommand cmd;
            Con.Open();
            byte[] image = null;
            //MessageBox.Show(ImageLocation);
            if (!string.IsNullOrEmpty(ImageLocation))
            {
                FileStream st = new FileStream(ImageLocation, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(st);
                image = br.ReadBytes((int)st.Length);
                st.Close();
                cmd = new SqlCommand("UPDATE icproduct SET productid=@productid, productname=@productname, categoryid=@categoryid, active=@active, notes=@notes, " +
                                    "price=@price, umid=@umid, sumid=@sumid, image=@image, updated_at=@updated_at WHERE productid=@PKey;", Con);
                cmd.Parameters.AddWithValue("@image", image);
            }
            else
            {
                String sql = "UPDATE icproduct SET productid=@productid, productname=@productname, categoryid=@categoryid, active=@active, notes=@notes, " +
                "price=@price, umid=@umid, sumid=@sumid, updated_at=@updated_at WHERE productid=@PKey;";
                if(imagePro.Image == null || imagePro.Image == Default_img)
                {
                    sql = "UPDATE icproduct SET productid=@productid, productname=@productname, categoryid=@categoryid, active=@active, notes=@notes, " +
                    "price=@price, umid=@umid, sumid=@sumid, image=NULL, updated_at=@updated_at WHERE productid=@PKey;";
                }
                cmd = new SqlCommand(sql, Con);
            }
            int active = 0;
            if (checkActive.Checked == true)
            {
                active = 1;
            }
            //MessageBox.Show(Key.ToString());

            cmd.Parameters.AddWithValue("@productid", txtProID.Text);
            cmd.Parameters.AddWithValue("@productname", txtProName.Text);
            cmd.Parameters.AddWithValue("@price", txtPrice.Text);
            cmd.Parameters.AddWithValue("@categoryid", cboCateID.Text);
            cmd.Parameters.AddWithValue("@umid", cboumid.Text);
            cmd.Parameters.AddWithValue("@sumid", cbosumid.Text);
            cmd.Parameters.AddWithValue("@notes", txtNote.Text);
            cmd.Parameters.AddWithValue("@active", active);
            cmd.Parameters.AddWithValue("@updated_at", up_date.Value);
            cmd.Parameters.AddWithValue("@PKey", Key);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Product Updated!");
                Key = txtProID.Text;
            }
            catch (Exception ex)
            {
                Con.Close();
                MessageBox.Show(ex.Message);
            }
            if (Con.State == ConnectionState.Open)
                Con.Close();


            Refresh();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new frmAddPro(dgvPro).Show();
            //Refresh();
        }

        private void btnPic_Click(object sender, EventArgs e)
        {
            dialog = new();
            dialog.Filter = "Select  image(*.Jpg; *.png; *.Gif)|*.Jpg; *.png; *.Gif";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                //imagePro.Image = Image.FromFile(dialog.FileName);
                this.ImageLocation = dialog.FileName.ToString();
                //MessageBox.Show(dialog.FileName.ToString());
                imagePro.ImageLocation = ImageLocation;
            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void frmProduct_Load(object sender, EventArgs e)
        {
            if (imagePro.Image == null)
            {
                imagePro.Image = Image.FromFile(@"C:\Users\radyk\Downloads\no-image.png");
                imagePro.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            /*Con.Open();
            String q = ("SELECT productid,image, productname, categoryid,active, notes, price, umid, sumid FROM icproduct");
            SqlCommand ss =new SqlCommand(q, Con);
            SqlDataAdapter sql = new SqlDataAdapter(q, Con);
            DataTable dt = new DataTable();
            sql.Fill(dt);
            dgvPro.DataSource = dt;
            DataGridViewImageColumn imageColumn = new DataGridViewImageColumn();
            imageColumn = (DataGridViewImageColumn)dgvPro.Columns[2];
            imageColumn.ImageLayout = DataGridViewImageCellLayout.Stretch;

            Con.Close();*/
        }

        private void dgvPro_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnRomove_Click(object sender, EventArgs e)
        {
            Remove_Image();
        }
        private void Remove_Image()
        {
            byte[] image = null;

            if (imagePro.Image == null)
            {
                MessageBox.Show("No image to remove!");
            }
            else
            {
                if (MessageBox.Show("Do you want to remove product image?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //imagePro.Image = null;
                    imagePro.Image = Default_img;
                    imagePro.SizeMode = PictureBoxSizeMode.Zoom;
                }
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Refresh();
        }
    }
}
