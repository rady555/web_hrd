﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace POS_Csharp.Forms
{
    public partial class frmCategory : Form
    {

        public frmCategory()
        {
            InitializeComponent();
            Reset();
            DisplayCate();
            Refresh();
            Count_rows();

        }

        SqlConnection Con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        private void DisplayCate()
        {
            int count = 0;
            Con.Open();
            String q = ("SELECT categoryid, categoryname, notes, active FROM iccategory");
            SqlCommand sqli = new SqlCommand(q, Con);
            SqlDataAdapter sql = new SqlDataAdapter(q, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            while (reader.Read())
            {
                dgvCate.Rows.Add(reader["categoryid"], reader["categoryname"], reader["notes"], reader["active"]);
                dgvCate.RowHeadersVisible = false;
                //count = dgvCate.RowCount;
                //lblCount.Text = count.ToString() + " records";
            }
            Con.Close();
        }

        public int Count_rows()
        {
            int count = 0;
            Con.Open();
            string stmt = "SELECT COUNT(*) FROM dbo.iccategory";
            SqlCommand cmdCount = new SqlCommand(stmt, Con);
            count = (int)cmdCount.ExecuteScalar();
            Con.Close();
            lblCount.Text = count.ToString() + " records";
            return count;
        }

        private void Refresh()
        {
            Con.Open();
            String q = ("SELECT categoryid, categoryname, notes, active FROM iccategory WHERE categoryid like'" + this.txtSearch.Text + "%' OR categoryname like'" + this.txtSearch.Text + "%'");
            SqlCommand sqli = new SqlCommand(q, Con);
            SqlDataAdapter sql = new SqlDataAdapter(q, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            dgvCate.Rows.Clear();
            while (reader.Read())
            {
                dgvCate.Rows.Add(reader["categoryid"], reader["categoryname"], reader["notes"], reader["active"]);
                dgvCate.RowHeadersVisible = false;
            }
            Con.Close();
        }

        private void Reset()
        {
            txtCateID.Text = "";
            txtCategoryName.Text = "";
            txtNote.Text = "";
            checkActive.Checked = false;
        }

        String Key = "";
        private void dgvCate_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                this.Key = dgvCate.Rows[e.RowIndex].Cells[0].Value.ToString();
                try
                {
                    txtCateID.Text = dgvCate.Rows[e.RowIndex].Cells[0].Value.ToString();
                    txtCategoryName.Text = dgvCate.Rows[e.RowIndex].Cells[1].Value.ToString();
                    txtNote.Text = dgvCate.Rows[e.RowIndex].Cells[2].Value.ToString();
                    if (txtCateID.Text == "")
                    {
                        Key = "";
                    }
                    else
                    {
                        // MessageBox.Show(dgvCategory.Rows[e.RowIndex].Cells[0].Value.ToString());
                        Key = dgvCate.Rows[e.RowIndex].Cells[0].Value.ToString();
                    }

                    if (dgvCate.Rows[e.RowIndex].Cells[3].Value.ToString() == "1")
                    {
                        checkActive.Checked = true;
                    }
                    else
                    {
                        checkActive.Checked = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                Reset();
            }
        }

        private void dgvCate_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                this.Key = dgvCate.Rows[e.RowIndex].Cells[0].Value.ToString();
                try
                {
                    txtCateID.Text = dgvCate.Rows[e.RowIndex].Cells[0].Value.ToString();
                    txtCategoryName.Text = dgvCate.Rows[e.RowIndex].Cells[1].Value.ToString();
                    txtNote.Text = dgvCate.Rows[e.RowIndex].Cells[2].Value.ToString();
                    if (txtCateID.Text == "")
                    {
                        Key = "";
                    }
                    else
                    {
                        Key = dgvCate.Rows[e.RowIndex].Cells[0].Value.ToString();
                    }

                    if (dgvCate.Rows[e.RowIndex].Cells[3].Value.ToString() == "1")
                    {
                        checkActive.Checked = true;
                    }
                    else
                    {
                        checkActive.Checked = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                Reset();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("update iccategory set categoryid=@categoryid, categoryname=@categoryname, notes=@notes, active=@active where categoryid=@PKey;", Con);
            Con.Open();
            int active = 0;
            if (checkActive.Checked == true)
            {
                active = 1;
            }
            cmd.Parameters.AddWithValue("@categoryid", txtCateID.Text.ToString());
            cmd.Parameters.AddWithValue("@categoryname", txtCategoryName.Text);
            cmd.Parameters.AddWithValue("@notes", txtNote.Text);
            cmd.Parameters.AddWithValue("@active", active);
            cmd.Parameters.AddWithValue("@PKey", Key);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            MessageBox.Show("Category Updated!");
            Con.Close();
            Refresh();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Key == "")
            {
                MessageBox.Show("Please select Category!");
            }
            else
            {
                if (MessageBox.Show("Do you want to delete this record?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        Con.Open();
                        SqlCommand cmd = new SqlCommand("delete FROM iccategory where categoryname = @categoryname", Con);
                        cmd.Parameters.AddWithValue("@categoryname", txtCategoryName.Text);

                        cmd.ExecuteNonQuery();
                        Con.Close();
                        MessageBox.Show("Category Deleted!");
                        Refresh();
                        Reset();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
             new frmAddCate(dgvCate).Show();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void checkActive_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
