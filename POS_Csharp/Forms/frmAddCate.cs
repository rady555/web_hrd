﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;


namespace POS_Csharp.Forms
{
    public partial class frmAddCate : Form
    {
        private DataGridView dgvCate = null;

        public frmAddCate(DataGridView dgvCate)
        {
            InitializeComponent();
            this.dgvCate = dgvCate;
            Reset();
            Refresh();
        }

        SqlConnection con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");


        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);


        protected override void WndProc(ref Message m)
        {
            const int WM_NCCALCSIZE = 0x0083;
            if (m.Msg == WM_NCCALCSIZE && m.WParam.ToInt32() == 1)
            {
                return;
            }
            base.WndProc(ref m);
        }

        private void Refresh()
        {
            con.Open();
            String q = ("SELECT categoryid, categoryname, notes, active FROM iccategory");
            SqlCommand sqli = new SqlCommand(q, con);
            SqlDataAdapter sql = new SqlDataAdapter(q, con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            dgvCate.Rows.Clear();
            while (reader.Read())
            {
                dgvCate.Rows.Add(reader["categoryid"], reader["categoryname"], reader["notes"], reader["active"]);
                dgvCate.RowHeadersVisible = false;
            }
            con.Close();
        }

        private void Reset()
        {
            txtCateID.Text = "";
            txtCategoryName.Text = "";
            txtNote.Text = "";
            checkActive.Checked = false;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtCateID.Clear();
            txtCategoryName.Clear();
            txtNote.Clear();
            checkActive.Checked = false;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtCateID.Text == "")
            {
                MessageBox.Show("Please input Category ID!");
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");
                con.Open();
                SqlCommand cmd = new SqlCommand("insert into iccategory ( categoryid,categoryname, notes,active) values (@categoryid, @categoryname, @notes, @active)", con);
                int active = 0;
                if (checkActive.Checked == true)
                {
                    active = 1;
                }
                cmd.Parameters.AddWithValue("@categoryid", txtCateID.Text);
                cmd.Parameters.AddWithValue("@categoryname", txtCategoryName.Text);
                cmd.Parameters.AddWithValue("@notes", txtNote.Text);
                cmd.Parameters.AddWithValue("@active", active);
                try
                {
                    cmd.ExecuteNonQuery();
                    //MessageBox.Show("Category Added!");
                    //cateid();
                    //Reset();
                    //Refresh();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                if (MessageBox.Show("Do you want to add more record?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Reset();
                    Refresh();
                }
                else
                {
                    this.Close();
                    Refresh();
                    con.Close();
                }
            }
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
