﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace POS_Csharp.Forms
{
    public partial class frmInvoice : Form
    {
        public static frmInvoice instance;
        public Label Invoice;
        public Label Date_Inv;
        public Label Customer;
        public Label Cashier;
        public DataGridView dgv_Inv;
        public Label Received_Dollar;
        public Label Received_Riel;
        public Label Subtotal_Dollar;
        public Label Subtotal_Riel;
        public Label TotalDiscount_Dollar;
        public Label TotalDiscount_Riel;
        public Label TotalAmount_Dollar;
        public Label TotalAmount_Riel;
        public Label Change_Dollar;
        public Label Change_Riel;

        public frmInvoice(DataGridView dgv)
        {
            InitializeComponent();
            instance = this;
            Invoice = lbl_invoice;
            Date_Inv = lbl_Date;
            Customer = lbl_Cus;
            Cashier = lbl_Cash;
            //dgv_Inv = dataGridView_Inv;
            Received_Dollar = new Label();
            Received_Riel = new Label();
            Subtotal_Dollar = lbl_SubDollar;
            Subtotal_Riel = new Label();
            TotalDiscount_Dollar = lbl_TotalDis_Dollar;
            TotalDiscount_Riel = new Label();
            TotalAmount_Dollar = lbl_TotalAmount_Dollar;
            TotalAmount_Riel = lbl_TotalAmount_Riel;
            Change_Dollar = lbl_ChangeDollar;
            Change_Riel = lbl_ChangeRiel;
            //MessageBox.Show(dgv_panel.Height.ToString());
            int height = dgv.RowCount * dgv.RowTemplate.Height;
            //MessageBox.Show(height.ToString());
            center_panel.Height += height;
            dgv_panel.Height += height;
            //MessageBox.Show(dgv_panel.Height.ToString());
            int i = 0;
            foreach(DataGridViewRow row in dgv.Rows)
            {

                dataGridView_Inv.Rows.Add(
                    (++i).ToString(),
                    row.Cells[0].Value.ToString(),
                    row.Cells[3].Value.ToString(),
                    row.Cells[4].Value.ToString(),
                    row.Cells[5].Value.ToString(),
                    row.Cells[6].Value.ToString()
                );
            }
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);

        private void iconButton1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnPrinf, "Print");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        protected override void WndProc(ref Message m)
        {
            const int WM_NCCALCSIZE = 0x0083;
            if (m.Msg == WM_NCCALCSIZE && m.WParam.ToInt32() == 1)
            {
                return;
            }
            base.WndProc(ref m);
        }

        private void lbl_Date_Click(object sender, EventArgs e)
        {
        }

        private void frmInvoice_Load(object sender, EventArgs e)
        {

            float ex_rate = 4000;
            lbl_Date.Text = DateTime.Now.ToString();

            string input1 = Received_Dollar.Text.Trim();
            string input2 = Received_Riel.Text.Trim();
            double.TryParse(input1, out double re_usd);
            double.TryParse(input2, out double re_riel);

            string input3 = TotalDiscount_Riel.Text.Replace("$", "");
            string input4 = Subtotal_Riel.Text.Replace("$", "");
            double.TryParse(input3, out double dis_R);
            double.TryParse(input4, out double sub_riel);

            //MessageBox.Show("Dis R: "+ dis_R.ToString());
            //MessageBox.Show("Sub R: "+ sub_riel.ToString());

            double total_r = re_riel / ex_rate;
            double total_d = re_usd + total_r;
            double con_riel = total_d * ex_rate;

            double sub_Riel = sub_riel * ex_rate;
            double dis_Riel = dis_R * ex_rate;

            lbl_RecDollar.Text = "$"+total_d.ToString("$#,##0.00#,##").Replace("$", "");
            lbl_RecRiel.Text = con_riel.ToString("#,##0.###").Replace("Riel", "") + "Riel";

            lbl_SubRiel.Text = sub_Riel.ToString("#,##0.###") + "Riel";
            lbl_TotalDis_Riel.Text = dis_Riel.ToString("#,##0.###") + "Riel";
            

        }
        private void Print(Panel pnl)
        {
            PrinterSettings ps = new PrinterSettings();
            center_panel = pnl;
            getprintarea(pnl);
            printPreviewDialog1.Document = printDocument;
            printDocument.PrintPage += new PrintPageEventHandler(printDocument_PrintPage);
            printPreviewDialog1.ShowDialog();
        }
        private Bitmap memoryimg;
        private void getprintarea(Panel pnl)
        {
            memoryimg = new Bitmap(pnl.Width, pnl.Height);
            pnl.DrawToBitmap(memoryimg, new Rectangle(0, 0, pnl.Width, pnl.Height));
        }

        private void printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            Rectangle pagearea = e.PageBounds;
            e.Graphics.DrawImage(memoryimg,(pagearea.Width/2) - (this.center_panel.Width/2), this.center_panel.Location.Y);
        }

        private void btnPrinf_Click(object sender, EventArgs e)
        {
            Print(this.center_panel);
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView_Inv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
