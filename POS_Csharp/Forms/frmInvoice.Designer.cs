﻿namespace POS_Csharp.Forms
{
    partial class frmInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInvoice));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new FontAwesome.Sharp.IconButton();
            this.btnPrinf = new FontAwesome.Sharp.IconButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.center_panel = new System.Windows.Forms.Panel();
            this.dgv_panel = new System.Windows.Forms.Panel();
            this.dataGridView_Inv = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl_Date = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.lbl_invoice = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lbl_Cus = new System.Windows.Forms.Label();
            this.lbl_Cash = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl_SubDollar = new System.Windows.Forms.Label();
            this.lbl_TotalDis_Dollar = new System.Windows.Forms.Label();
            this.lbl_TotalAmount_Dollar = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbl_ChangeDollar = new System.Windows.Forms.Label();
            this.lbl_RecRiel = new System.Windows.Forms.Label();
            this.lbl_SubRiel = new System.Windows.Forms.Label();
            this.lbl_RecDollar = new System.Windows.Forms.Label();
            this.lbl_TotalDis_Riel = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbl_TotalAmount_Riel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_ChangeRiel = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.txtChange_rate = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.printDocument = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.container_panel = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.center_panel.SuspendLayout();
            this.dgv_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Inv)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.container_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnPrinf);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(834, 51);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnClose.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnClose.IconColor = System.Drawing.Color.Black;
            this.btnClose.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnClose.Location = new System.Drawing.Point(769, 7);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(58, 38);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "OK";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPrinf
            // 
            this.btnPrinf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrinf.FlatAppearance.BorderSize = 0;
            this.btnPrinf.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnPrinf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrinf.IconChar = FontAwesome.Sharp.IconChar.Print;
            this.btnPrinf.IconColor = System.Drawing.SystemColors.HotTrack;
            this.btnPrinf.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnPrinf.IconSize = 40;
            this.btnPrinf.Location = new System.Drawing.Point(724, 7);
            this.btnPrinf.Name = "btnPrinf";
            this.btnPrinf.Size = new System.Drawing.Size(43, 39);
            this.btnPrinf.TabIndex = 0;
            this.btnPrinf.UseVisualStyleBackColor = true;
            this.btnPrinf.Click += new System.EventHandler(this.btnPrinf_Click);
            this.btnPrinf.MouseHover += new System.EventHandler(this.iconButton1_MouseHover);
            // 
            // center_panel
            // 
            this.center_panel.Controls.Add(this.dgv_panel);
            this.center_panel.Controls.Add(this.panel6);
            this.center_panel.Controls.Add(this.panel5);
            this.center_panel.Controls.Add(this.panel4);
            this.center_panel.Controls.Add(this.panel3);
            this.center_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.center_panel.Location = new System.Drawing.Point(5, 5);
            this.center_panel.Name = "center_panel";
            this.center_panel.Size = new System.Drawing.Size(824, 508);
            this.center_panel.TabIndex = 2;
            // 
            // dgv_panel
            // 
            this.dgv_panel.Controls.Add(this.dataGridView_Inv);
            this.dgv_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgv_panel.Location = new System.Drawing.Point(0, 242);
            this.dgv_panel.Name = "dgv_panel";
            this.dgv_panel.Padding = new System.Windows.Forms.Padding(65, 0, 65, 0);
            this.dgv_panel.Size = new System.Drawing.Size(824, 50);
            this.dgv_panel.TabIndex = 52;
            // 
            // dataGridView_Inv
            // 
            this.dataGridView_Inv.AllowUserToAddRows = false;
            this.dataGridView_Inv.AllowUserToDeleteRows = false;
            this.dataGridView_Inv.AllowUserToResizeColumns = false;
            this.dataGridView_Inv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dataGridView_Inv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_Inv.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_Inv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_Inv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            this.dataGridView_Inv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Red;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_Inv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_Inv.ColumnHeadersHeight = 35;
            this.dataGridView_Inv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView_Inv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.Padding = new System.Windows.Forms.Padding(3);
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_Inv.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView_Inv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_Inv.GridColor = System.Drawing.Color.Black;
            this.dataGridView_Inv.Location = new System.Drawing.Point(65, 0);
            this.dataGridView_Inv.Name = "dataGridView_Inv";
            this.dataGridView_Inv.ReadOnly = true;
            this.dataGridView_Inv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Red;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_Inv.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView_Inv.RowHeadersVisible = false;
            this.dataGridView_Inv.RowHeadersWidth = 35;
            this.dataGridView_Inv.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dataGridView_Inv.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dataGridView_Inv.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView_Inv.RowTemplate.Height = 25;
            this.dataGridView_Inv.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView_Inv.Size = new System.Drawing.Size(694, 50);
            this.dataGridView_Inv.TabIndex = 16;
            this.dataGridView_Inv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Inv_CellContentClick);
            // 
            // Column1
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.HeaderText = " No";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Items";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 250;
            // 
            // Column3
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column3.HeaderText = "Qty";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 60;
            // 
            // Column4
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column4.HeaderText = "Discount";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 95;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Price";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Amount";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 125;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.lbl_Date);
            this.panel6.Controls.Add(this.label);
            this.panel6.Controls.Add(this.lbl_invoice);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.lbl_Cus);
            this.panel6.Controls.Add(this.lbl_Cash);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 77);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(824, 165);
            this.panel6.TabIndex = 55;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(554, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(203, 60);
            this.label5.TabIndex = 15;
            this.label5.Text = "               No. 73BG, Street 360, \r\n Sangkat Boeung Keng Kang III\r\n Phnom Penh" +
    ", Cambodia 12304\r\n";
            // 
            // lbl_Date
            // 
            this.lbl_Date.AutoSize = true;
            this.lbl_Date.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_Date.Location = new System.Drawing.Point(202, 71);
            this.lbl_Date.Name = "lbl_Date";
            this.lbl_Date.Size = new System.Drawing.Size(45, 20);
            this.lbl_Date.TabIndex = 17;
            this.lbl_Date.Text = "label6";
            this.lbl_Date.Click += new System.EventHandler(this.lbl_Date_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label.Location = new System.Drawing.Point(76, 41);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(76, 20);
            this.label.TabIndex = 10;
            this.label.Text = "Invoice_No";
            // 
            // lbl_invoice
            // 
            this.lbl_invoice.AutoSize = true;
            this.lbl_invoice.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_invoice.ForeColor = System.Drawing.Color.Black;
            this.lbl_invoice.Location = new System.Drawing.Point(202, 41);
            this.lbl_invoice.Name = "lbl_invoice";
            this.lbl_invoice.Size = new System.Drawing.Size(52, 20);
            this.lbl_invoice.TabIndex = 11;
            this.lbl_invoice.Text = "Invoice";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label15.Location = new System.Drawing.Point(178, 41);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 20);
            this.label15.TabIndex = 51;
            this.label15.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(76, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 12;
            this.label2.Text = "DateTime";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label21.Location = new System.Drawing.Point(178, 132);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(13, 20);
            this.label21.TabIndex = 50;
            this.label21.Text = ":";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(76, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 20);
            this.label3.TabIndex = 13;
            this.label3.Text = "Customer";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label22.Location = new System.Drawing.Point(178, 102);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 20);
            this.label22.TabIndex = 49;
            this.label22.Text = ":";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(76, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 20);
            this.label4.TabIndex = 14;
            this.label4.Text = "Cashier";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label24.Location = new System.Drawing.Point(178, 71);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(13, 20);
            this.label24.TabIndex = 47;
            this.label24.Text = ":";
            // 
            // lbl_Cus
            // 
            this.lbl_Cus.AutoSize = true;
            this.lbl_Cus.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_Cus.Location = new System.Drawing.Point(202, 102);
            this.lbl_Cus.Name = "lbl_Cus";
            this.lbl_Cus.Size = new System.Drawing.Size(45, 20);
            this.lbl_Cus.TabIndex = 18;
            this.lbl_Cus.Text = "label7";
            // 
            // lbl_Cash
            // 
            this.lbl_Cash.AutoSize = true;
            this.lbl_Cash.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_Cash.Location = new System.Drawing.Point(202, 132);
            this.lbl_Cash.Name = "lbl_Cash";
            this.lbl_Cash.Size = new System.Drawing.Size(45, 20);
            this.lbl_Cash.TabIndex = 19;
            this.lbl_Cash.Text = "label8";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(824, 77);
            this.panel5.TabIndex = 54;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(353, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Invoice";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.lbl_SubDollar);
            this.panel4.Controls.Add(this.lbl_TotalDis_Dollar);
            this.panel4.Controls.Add(this.lbl_TotalAmount_Dollar);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.lbl_ChangeDollar);
            this.panel4.Controls.Add(this.lbl_RecRiel);
            this.panel4.Controls.Add(this.lbl_SubRiel);
            this.panel4.Controls.Add(this.lbl_RecDollar);
            this.panel4.Controls.Add(this.lbl_TotalDis_Riel);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.lbl_TotalAmount_Riel);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.lbl_ChangeRiel);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 292);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(824, 127);
            this.panel4.TabIndex = 53;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(402, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 20);
            this.label7.TabIndex = 21;
            this.label7.Text = "Total Discount";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(402, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 20);
            this.label6.TabIndex = 20;
            this.label6.Text = "Subtotal ";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(402, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 20);
            this.label8.TabIndex = 22;
            this.label8.Text = "Total Amount";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label9.Location = new System.Drawing.Point(402, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 20);
            this.label9.TabIndex = 23;
            this.label9.Text = "Change";
            // 
            // lbl_SubDollar
            // 
            this.lbl_SubDollar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_SubDollar.AutoSize = true;
            this.lbl_SubDollar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_SubDollar.Location = new System.Drawing.Point(544, 32);
            this.lbl_SubDollar.Name = "lbl_SubDollar";
            this.lbl_SubDollar.Size = new System.Drawing.Size(16, 20);
            this.lbl_SubDollar.TabIndex = 24;
            this.lbl_SubDollar.Text = "$";
            // 
            // lbl_TotalDis_Dollar
            // 
            this.lbl_TotalDis_Dollar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_TotalDis_Dollar.AutoSize = true;
            this.lbl_TotalDis_Dollar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_TotalDis_Dollar.Location = new System.Drawing.Point(544, 52);
            this.lbl_TotalDis_Dollar.Name = "lbl_TotalDis_Dollar";
            this.lbl_TotalDis_Dollar.Size = new System.Drawing.Size(53, 20);
            this.lbl_TotalDis_Dollar.TabIndex = 25;
            this.lbl_TotalDis_Dollar.Text = "label11";
            // 
            // lbl_TotalAmount_Dollar
            // 
            this.lbl_TotalAmount_Dollar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_TotalAmount_Dollar.AutoSize = true;
            this.lbl_TotalAmount_Dollar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_TotalAmount_Dollar.Location = new System.Drawing.Point(544, 72);
            this.lbl_TotalAmount_Dollar.Name = "lbl_TotalAmount_Dollar";
            this.lbl_TotalAmount_Dollar.Size = new System.Drawing.Size(53, 20);
            this.lbl_TotalAmount_Dollar.TabIndex = 26;
            this.lbl_TotalAmount_Dollar.Text = "label12";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label11.Location = new System.Drawing.Point(622, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 20);
            this.label11.TabIndex = 39;
            this.label11.Text = ":";
            // 
            // lbl_ChangeDollar
            // 
            this.lbl_ChangeDollar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_ChangeDollar.AutoSize = true;
            this.lbl_ChangeDollar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_ChangeDollar.Location = new System.Drawing.Point(544, 92);
            this.lbl_ChangeDollar.Name = "lbl_ChangeDollar";
            this.lbl_ChangeDollar.Size = new System.Drawing.Size(53, 20);
            this.lbl_ChangeDollar.TabIndex = 27;
            this.lbl_ChangeDollar.Text = "label13";
            // 
            // lbl_RecRiel
            // 
            this.lbl_RecRiel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_RecRiel.AutoSize = true;
            this.lbl_RecRiel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_RecRiel.Location = new System.Drawing.Point(655, 12);
            this.lbl_RecRiel.Name = "lbl_RecRiel";
            this.lbl_RecRiel.Size = new System.Drawing.Size(40, 20);
            this.lbl_RecRiel.TabIndex = 38;
            this.lbl_RecRiel.Text = "0Riel";
            // 
            // lbl_SubRiel
            // 
            this.lbl_SubRiel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_SubRiel.AutoSize = true;
            this.lbl_SubRiel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_SubRiel.Location = new System.Drawing.Point(655, 32);
            this.lbl_SubRiel.Name = "lbl_SubRiel";
            this.lbl_SubRiel.Size = new System.Drawing.Size(40, 20);
            this.lbl_SubRiel.TabIndex = 28;
            this.lbl_SubRiel.Text = "0Riel";
            // 
            // lbl_RecDollar
            // 
            this.lbl_RecDollar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_RecDollar.AutoSize = true;
            this.lbl_RecDollar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_RecDollar.Location = new System.Drawing.Point(544, 12);
            this.lbl_RecDollar.Name = "lbl_RecDollar";
            this.lbl_RecDollar.Size = new System.Drawing.Size(16, 20);
            this.lbl_RecDollar.TabIndex = 37;
            this.lbl_RecDollar.Text = "$";
            // 
            // lbl_TotalDis_Riel
            // 
            this.lbl_TotalDis_Riel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_TotalDis_Riel.AutoSize = true;
            this.lbl_TotalDis_Riel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_TotalDis_Riel.Location = new System.Drawing.Point(655, 52);
            this.lbl_TotalDis_Riel.Name = "lbl_TotalDis_Riel";
            this.lbl_TotalDis_Riel.Size = new System.Drawing.Size(53, 20);
            this.lbl_TotalDis_Riel.TabIndex = 29;
            this.lbl_TotalDis_Riel.Text = "label16";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label14.Location = new System.Drawing.Point(402, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 20);
            this.label14.TabIndex = 36;
            this.label14.Text = "Received";
            // 
            // lbl_TotalAmount_Riel
            // 
            this.lbl_TotalAmount_Riel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_TotalAmount_Riel.AutoSize = true;
            this.lbl_TotalAmount_Riel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_TotalAmount_Riel.Location = new System.Drawing.Point(655, 72);
            this.lbl_TotalAmount_Riel.Name = "lbl_TotalAmount_Riel";
            this.lbl_TotalAmount_Riel.Size = new System.Drawing.Size(53, 20);
            this.lbl_TotalAmount_Riel.TabIndex = 30;
            this.lbl_TotalAmount_Riel.Text = "label15";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label10.Location = new System.Drawing.Point(622, 92);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 20);
            this.label10.TabIndex = 35;
            this.label10.Text = ":";
            // 
            // lbl_ChangeRiel
            // 
            this.lbl_ChangeRiel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbl_ChangeRiel.AutoSize = true;
            this.lbl_ChangeRiel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_ChangeRiel.Location = new System.Drawing.Point(655, 92);
            this.lbl_ChangeRiel.Name = "lbl_ChangeRiel";
            this.lbl_ChangeRiel.Size = new System.Drawing.Size(53, 20);
            this.lbl_ChangeRiel.TabIndex = 31;
            this.lbl_ChangeRiel.Text = "label14";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label17.Location = new System.Drawing.Point(622, 72);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 20);
            this.label17.TabIndex = 34;
            this.label17.Text = ":";
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label19.Location = new System.Drawing.Point(622, 32);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 20);
            this.label19.TabIndex = 32;
            this.label19.Text = ":";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label18.Location = new System.Drawing.Point(622, 52);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 20);
            this.label18.TabIndex = 33;
            this.label18.Text = ":";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.txtChange_rate);
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 419);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(824, 89);
            this.panel3.TabIndex = 52;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.White;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label12.Location = new System.Drawing.Point(76, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 20);
            this.label12.TabIndex = 40;
            this.label12.Text = "Exchange rate:";
            // 
            // txtChange_rate
            // 
            this.txtChange_rate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtChange_rate.BackColor = System.Drawing.Color.White;
            this.txtChange_rate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChange_rate.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtChange_rate.Location = new System.Drawing.Point(229, 4);
            this.txtChange_rate.Name = "txtChange_rate";
            this.txtChange_rate.ReadOnly = true;
            this.txtChange_rate.Size = new System.Drawing.Size(41, 19);
            this.txtChange_rate.TabIndex = 41;
            this.txtChange_rate.Text = "4000";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBox1.Location = new System.Drawing.Point(174, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(49, 19);
            this.textBox1.TabIndex = 42;
            this.textBox1.Text = "1$  =";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label20.Location = new System.Drawing.Point(76, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 20);
            this.label20.TabIndex = 43;
            this.label20.Text = "Note:";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(114, 24);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(320, 20);
            this.label16.TabIndex = 44;
            this.label16.Text = "Goods once sold cannot be returned or exchanged!\r\n";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(584, 43);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(170, 20);
            this.label13.TabIndex = 45;
            this.label13.Text = "Thank you and come again";
            // 
            // printDocument
            // 
            this.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // container_panel
            // 
            this.container_panel.AutoScroll = true;
            this.container_panel.Controls.Add(this.center_panel);
            this.container_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.container_panel.Location = new System.Drawing.Point(0, 51);
            this.container_panel.Name = "container_panel";
            this.container_panel.Padding = new System.Windows.Forms.Padding(5);
            this.container_panel.Size = new System.Drawing.Size(834, 517);
            this.container_panel.TabIndex = 56;
            // 
            // frmInvoice
            // 
            this.AcceptButton = this.btnPrinf;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(834, 568);
            this.Controls.Add(this.container_panel);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmInvoice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmInvoice";
            this.Load += new System.EventHandler(this.frmInvoice_Load);
            this.panel1.ResumeLayout(false);
            this.center_panel.ResumeLayout(false);
            this.dgv_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Inv)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.container_panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private FontAwesome.Sharp.IconButton btnPrinf;
        private ToolTip toolTip1;
        private Panel center_panel;
        private Label label1;
        private Label label;
        private Label lbl_invoice;
        private Label label3;
        private Label label2;
        private Label label4;
        private Label label5;
        private DataGridView dataGridView_Inv;
        private Label lbl_Cash;
        private Label lbl_Cus;
        private Label lbl_Date;
        private Label label9;
        private Label label8;
        private Label label7;
        private Label label6;
        private Label lbl_ChangeDollar;
        private Label lbl_TotalAmount_Dollar;
        private Label lbl_TotalDis_Dollar;
        private Label lbl_SubDollar;
        private Label lbl_ChangeRiel;
        private Label lbl_TotalAmount_Riel;
        private Label lbl_TotalDis_Riel;
        private Label lbl_SubRiel;
        private Label label10;
        private Label label17;
        private Label label18;
        private Label label19;
        private Label label11;
        private Label lbl_RecRiel;
        private Label lbl_RecDollar;
        private Label label14;
        private TextBox textBox1;
        private TextBox txtChange_rate;
        private Label label12;
        private Label label16;
        private Label label20;
        private Label label13;
        private FontAwesome.Sharp.IconButton btnClose;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn Column4;
        private DataGridViewTextBoxColumn Column5;
        private DataGridViewTextBoxColumn Column6;
        private Label label15;
        private Label label21;
        private Label label22;
        private Label label24;
        private System.Drawing.Printing.PrintDocument printDocument;
        private PrintPreviewDialog printPreviewDialog1;
        private Panel panel3;
        private Panel panel4;
        private Panel panel5;
        private Panel panel6;
        private Panel container_panel;
        private Panel dgv_panel;
    }
}