﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;


namespace POS_Csharp.Forms
{
    public partial class frmAddUM : Form
    {
        private DataGridView dgvUM = null;

        public frmAddUM(DataGridView dgvUM)
        {
            InitializeComponent();
            this.dgvUM = dgvUM;
            Reset();
            Refresh();
        }

        SqlConnection con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);


        protected override void WndProc(ref Message m)
        {
            const int WM_NCCALCSIZE = 0x0083;
            if (m.Msg == WM_NCCALCSIZE && m.WParam.ToInt32() == 1)
            {
                return;
            }
            base.WndProc(ref m);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtumid.Clear();
            txtumname.Clear();
            txtfactor.Clear();
            txtNote.Clear();
            checkActive.Checked = false;
        }
        private void Refresh()
        {
            con.Open();
            String q = ("SELECT umid, umname, notes, active,factor FROM icum");
            SqlCommand sqli = new SqlCommand(q, con);
            SqlDataAdapter sql = new SqlDataAdapter(q, con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            dgvUM.Rows.Clear();
            while (reader.Read())
            {
                dgvUM.Rows.Add(reader["umid"], reader["umname"], reader["factor"], reader["notes"], reader["active"]);
                dgvUM.RowHeadersVisible = false;
            }
            con.Close();
        }

        private void Reset()
        {
            txtumid.Text = "";
            txtumname.Text = "";
            txtfactor.Text = "";
            checkActive.Checked = false;
            txtNote.Text = "";
        }

        int key = 0;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtumid.Text == "")
            {
                key = 0;
                MessageBox.Show("Please input Unit of Measure ID!");
            }
            else
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("insert into icum (umid, umname, notes, active, factor) values (@umid, @umname, @notes, @active, @factor)", con);
                int active = 0;
                if (checkActive.Checked == true)
                {
                    active = 1;
                }

                cmd.Parameters.AddWithValue("@umid", txtumid.Text);
                cmd.Parameters.AddWithValue("@umname", txtumname.Text);
                cmd.Parameters.AddWithValue("@notes", txtNote.Text);
                cmd.Parameters.AddWithValue("@active", active);
                cmd.Parameters.AddWithValue("@factor", txtfactor.Text);
                try
                {
                    cmd.ExecuteNonQuery();
                    if (MessageBox.Show("Do you want to add more user?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Reset();
                        Refresh();
                    }
                    else
                    {
                        con.Close();
                        this.Close();
                        Refresh();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
    }
}
