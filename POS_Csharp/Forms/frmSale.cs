﻿using System.Data;
using System.Data.SqlClient;

namespace POS_Csharp.Forms
{
    public partial class frmSale : Form
    {
        public static frmSale instance;
        SqlDataReader dr;
        private PictureBox Picture;
        private Label lblPrice;
        private Label lblName;
        private TextBox txtproductid;
        private Label lblInv;
        private Image Default_img = null;


        public frmSale()
        {
            InitializeComponent();
            instance = this;
            lblInv = lblInv_No;
            Default_img = Image.FromFile(@"C:\Users\radyk\Downloads\no-image.png");

        }
        SqlConnection Con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");

        private void Sale_Load(object sender, EventArgs e)
        {
            //timer1.Start();
            GetData();
            invoiceID();
            Calculate();

            //if (Picture.Image == null)
            //{
            //    Picture.Image = Image.FromFile(@"C:\Users\radyk\Downloads\no-image.png");
            //    Picture.SizeMode = PictureBoxSizeMode.StretchImage;
            //}
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //date.Text = DateTime.Now.ToLongDateString();
            //time.Text = DateTime.Now.ToLongTimeString();
        }

        private void GetData()
        {
            Con.Open();
            flowLayoutPanel1.Controls.Clear();
            SqlCommand cmd = new SqlCommand("SELECT Top 40 image, productname, price, productid, sumid FROM icproduct WHERE productid like'" + this.txtSearch.Text + "%' OR productname like'" + this.txtSearch.Text + "%'", Con);
            //SqlDataReader dr = cmd.ExecuteReader();
            SqlDataAdapter sql = new SqlDataAdapter(cmd);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                long len = dr.GetBytes(0, 0, null, 0, 0);
                byte[] array = new byte[System.Convert.ToInt32(len) + 1];
                dr.GetBytes(0, 0, array, 0, System.Convert.ToInt32(len));

                Picture = new PictureBox();
                Panel panel = new Panel();
                panel.Width = 125;
                panel.Height = 165;
                panel.BorderStyle = BorderStyle.None;

                Picture.BackgroundImageLayout = ImageLayout.Stretch;
                Picture.BorderStyle = BorderStyle.None;
                Picture.Tag = dr["productid"].ToString();
                //Picture.Padding = new Padding(10, 0, 0, 0);

                txtproductid = new TextBox();
                txtproductid.Text = dr["productid"].ToString();
                txtproductid.Hide();

                lblPrice = new Label();
                lblPrice.Text = dr["price"].ToString();
                lblPrice.BackColor = Color.FromArgb(255, 255, 255, 255);
                lblPrice.BorderStyle = BorderStyle.None;
                lblPrice.TextAlign = ContentAlignment.MiddleCenter;
                lblPrice.Dock = DockStyle.Bottom;
                lblPrice.BackColor = Color.DarkSlateGray;
                lblPrice.ForeColor = Color.White;

                lblName = new Label();
                lblName.Text = dr["productname"].ToString();
                lblName.BackColor = Color.FromArgb(255, 255, 255, 255);
                lblName.BorderStyle = BorderStyle.None;
                lblName.TextAlign = ContentAlignment.MiddleCenter;
                lblName.Dock = DockStyle.Bottom;
                lblName.BackColor = Color.DarkSlateGray;
                lblName.ForeColor = Color.White;

                MemoryStream ms = new MemoryStream(array);
                Bitmap bitmap = new Bitmap(ms);
                Picture.BackgroundImage = bitmap;
                Picture.Height = panel.Height - (lblPrice.Height + lblName.Height);

                panel.Controls.Add(Picture);
                //flowLayoutPanel1.Controls.Add(Picture);
                panel.Controls.Add(lblPrice);
                panel.Controls.Add(lblName);
                lblPrice.Dock = DockStyle.Bottom;
                lblName.Dock = DockStyle.Bottom;
                Picture.Dock = DockStyle.Top;
                flowLayoutPanel1.Controls.Add(panel);
                Picture.Cursor = Cursors.Hand;

                panel.Click += new EventHandler(OnClick);
                Picture.Click += new EventHandler(OnClick);
                //lblPrice.Click += new EventHandler(OnClick);
                //lblName.Click += new EventHandler(OnClick);
                Calculate();
            }
            dr.Close();
            Con.Close();
        }
        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                if (e.KeyChar.ToString() != ".")
                    e.Handled = true;
            }
        }
        private void OnClick(object sender, EventArgs e)
        {
            int qty = 1;
            bool Found = false;
            float dis_percentage = 00;
            String tag = ((PictureBox)sender).Tag.ToString();
            Select_pro(tag);
        }
        private void Select_pro(String Code)
        {
            int qty = 1;
            bool Found = false;
            int count = 0;
            float dis_percentage = 00;
            Con.Open();
            SqlCommand cmd = new SqlCommand("SELECT productid, productname, price, sumid FROM icproduct WHERE productid like '" + Code + "'", Con);
            dr = cmd.ExecuteReader();
            if (dr.HasRows && dr.Read())
            {

                //    foreach (DataGridViewRow row in dataGridView1.Rows)
                //    {
                //        if (Convert.ToString(row.Cells[1].Value) == dr["productid"].ToString())
                //        {
                //            row.Cells[3].Value = Convert.ToString(1 + Convert.ToInt16(row.Cells[3].Value));
                //            double price = double.Parse(row.Cells[5].Value.ToString().Replace("$", ""));
                //            qty = int.Parse(row.Cells[3].Value.ToString());
                //            double discount = double.Parse(row.Cells[4].Value.ToString());
                //            row.Cells[6].Value = (price * qty).ToString("C2");

                //            double amount = double.Parse(row.Cells[6].Value.ToString().Replace("$", ""));
                //            dis_percentage = float.Parse(row.Cells[4].Value.ToString());
                //            amount -= (amount * dis_percentage / 100);
                //            row.Cells[6].Value = amount.ToString("C2");
                //            Found = true;
                //            Calculate();
                //            //row.Cells[2].Value = Convert.ToString(1 + Convert.ToInt16(row.Cells[2].Value));
                //            //row.Cells[5].Value = Convert.ToString(double.Parse(dr["price"].ToString()) * double.Parse(row.Cells[2].Value.ToString()));
                //            dr.Close();
                //            Con.Close();
                //            return;
                //        }
                //    }
                //    if (!Found)
                //    {
                //        dataGridView1.Rows.Add(dr["productname"].ToString(), dr["productid"].ToString(), dr["sumid"].ToString(), qty.ToString(), dis_percentage.ToString(), double.Parse(dr["price"].ToString()).ToString("$0.00#,##"), double.Parse(dr["price"].ToString()).ToString("C2"));
                //        //MessageBox.Show(dr["sumid"].ToString());
                //        count = dataGridView1.RowCount;
                //        lblCount.Text = "(" + count.ToString() + ")";
                //        Calculate();

                dataGridView1.Rows.Add(dr["productname"].ToString(), dr["productid"].ToString(), dr["sumid"].ToString(), qty.ToString(), dis_percentage.ToString(), double.Parse(dr["price"].ToString()).ToString("$0.00#,##"), double.Parse(dr["price"].ToString()).ToString("C2"));
                count = dataGridView1.RowCount;
                lblCount.Text = "(" + count.ToString() + ")";
                Calculate();
                dr.Close();
                Con.Close();
            }
        }

        int Key = 0;
        private void flowLayoutPanel1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        public void invoiceID()
        {
            Con.Open();
            string i = "No-";
            int n = 10000;
            string query = "select top 1 invid from posdetail order by invid desc";
            SqlCommand cc = new SqlCommand(query, Con);
            SqlDataReader dr = cc.ExecuteReader();
            if (!dr.HasRows)
            {
                lblInv_No.Text = "No-" + n.ToString();
            }
            else
            {
                dr.Read();
                string ss = dr["invid"].ToString();
                string[] aa = ss.Split("-");
                n = int.Parse(aa[1]) + 1;
                lblInv_No.Text = i + n.ToString();
            }
            dr.Close();
            Con.Close();

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to cancel order?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                dataGridView1.Rows.Clear();
            lblCount.Text = "(0)";
            Calculate();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Delete")
            {
                if (MessageBox.Show("Do you want to delete this record?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    dataGridView1.Rows.RemoveAt(e.RowIndex);
                Calculate();
            }
            if (dataGridView1.Columns[e.ColumnIndex].Name == "items" || dataGridView1.Columns[e.ColumnIndex].Name == "qty" || dataGridView1.Columns[e.ColumnIndex].Name == "price" || dataGridView1.Columns[e.ColumnIndex].Name == "amount")
            {
                //Key = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
                // MessageBox.Show("Hi");
            }
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void Calculate()
        {
            double Subtotal = 0;
            double Dis_Amount = 0;
            double Total = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                Subtotal += double.Parse(row.Cells[5].Value.ToString().Replace("$", "")) * int.Parse(row.Cells[3].Value.ToString());
                Total += double.Parse(row.Cells[6].Value.ToString().Replace("$", ""));
                Dis_Amount = Subtotal - Total;
            }
            lblSub.Text = Subtotal.ToString("C2");
            lblDis_Amount.Text = Dis_Amount.ToString("C2");
            lblTotal.Text = Total.ToString("C2");
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
         
            double dis_percentage = 0;
            double dis = 0;
            double amount_dis = 0;
            var row = dataGridView1.CurrentRow;
            double amount = (int.Parse(row.Cells[3].Value.ToString())) * (double.Parse(row.Cells[5].Value.ToString().Replace("$", "")));
            if (row.Cells[4].Value == null)
            {
                row.Cells[4].Value = "0";
                dis = 0;
            }
            else
            {
                dis = double.Parse(row.Cells[4].Value.ToString());
            }
            if (dis < 0 || dis > 100) {
                MessageBox.Show("Discount accept from 0% to 100%");
                row.Cells[4].Value = "0";
                dis = 0;
            }
            //else
            //{
            //    dis = double.Parse(row.Cells[4].Value.ToString());
            //}
            if (dataGridView1.Columns[e.ColumnIndex].HeaderText == "Qty")
            {
                amount = (int.Parse(dataGridView1.CurrentCell.Value.ToString())) * (double.Parse(row.Cells[5].Value.ToString().Replace("$", "")));
            }

            MessageBox.Show(dis.ToString());
            dis_percentage = dis;
            amount_dis = amount - (amount * dis_percentage / 100);
            row.Cells[6].Value = amount_dis + "$";
            Calculate();

        }

        private void panel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            instance_to_Cal();
            invoiceID();
        }
        private void instance_to_Cal()
        {
            int Ex_rate = 4000;
            int qty = 1;
            if (dataGridView1.Rows.Count <= 0)
            {
                MessageBox.Show("Please select product!", "Warn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                frmCal_Payment form = new frmCal_Payment(dataGridView1, lblCount, lblSub, lblDis_Amount, lblTotal);
                form.Show();

                frmCal_Payment.instance.txtTotalAmountD.Text = lblTotal.Text;
                frmCal_Payment.instance.lblInvoice = lblInv_No;
                frmCal_Payment.instance.lblCusID_Type.Text = txtCusType.Text;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    frmCal_Payment.instance.qty.Text = row.Cells[3].Value.ToString();
                    frmCal_Payment.instance.productid.Text = Convert.ToString(row.Cells[5].Value);
                    frmCal_Payment.instance.price.Text = row.Cells[5].Value.ToString();
                    frmCal_Payment.instance.sumid.Text = row.Cells[2].Value.ToString();
                    frmCal_Payment.instance.produdtname.Text = row.Cells[0].Value.ToString();
                }
            }

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            GetData();
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Select_pro(txtSearch.Text.Trim());
                txtSearch.Text = String.Empty;
            }
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dataGridView1.CurrentCell.ColumnIndex == 3 || dataGridView1.CurrentCell.ColumnIndex == 4)
            {
                if (e.Control != null && e.Control is TextBox)
                {
                    e.Control.KeyPress += delegate (object sender, KeyPressEventArgs MyE)
                    {
                        if (!char.IsDigit(MyE.KeyChar) && !char.IsControl(MyE.KeyChar))
                        {
                            MyE.Handled = true;
                        }
                    };
                }
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            //if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            //{
            //    if (e.KeyChar.ToString() != ".")
            //        e.Handled = true;
            //}
        }
    }
}
//try
//{
//    SqlCommand sql = new SqlCommand("INSERT INTO posdetail (invid, posdate, productid, productname, price, sumid)" +
//                        "VALUES (@invid, @posdate, @productid, @productname, @price, @sumid)", Con);

//    sql.Parameters.AddWithValue("@invid", txtInvoiceNo.ToString());
//    sql.Parameters.AddWithValue("@posdate", date.ToString());
//    sql.Parameters.AddWithValue("cusid", txtCusType.ToString());
//    sql.Parameters.AddWithValue("productid", lblid.ToString());
//    sql.Parameters.AddWithValue("productname", lblName.ToString());
//    sql.Parameters.AddWithValue("@qty", ToString());
//    sql.Parameters.AddWithValue("@price", lblPrice.ToString());
//    sql.Parameters.AddWithValue("@total_amount",.ToString());
//    sql.Parameters.AddWithValue("@sumid", Tag.ToString());
//    sql.ExecuteNonQuery();
//    invoiceID();
//    MessageBox.Show("Customer Added!");


//}
//catch (Exception ex)
//{
//    MessageBox.Show(ex.Message);

//}
//Con.Open();
//SqlCommand sql = new SqlCommand("INSERT INTO posdetail (invid, posdate, custid, productid, productname, qty, price, total_amount, sumid) " +
//    "VALUES (@invid, @posdate, @custid, @productid, @productname, @qty, @price, @total_amount, @sumid)", Con);
//    foreach(DataGridViewRow row in dataGridView1.Rows)
//    {
//    sql.Parameters.AddWithValue("@invid", lblInv_No.Text);
//    MessageBox.Show(lblInv_No.ToString());
//    sql.Parameters.AddWithValue("@posdate", date.Value);
//    sql.Parameters.AddWithValue("@custid", txtCusType.Text);
//    sql.Parameters.AddWithValue("@productid", row.Cells[1].Value.ToString());
//    sql.Parameters.AddWithValue("@productname", lblName.Text);
//    sql.Parameters.AddWithValue("@qty", int.Parse(row.Cells[3].Value.ToString()));
//    sql.Parameters.AddWithValue("@price", lblPrice.Text);
//    sql.Parameters.AddWithValue("@total_amount", lblTotal.Text);
//    sql.Parameters.AddWithValue("@sumid", row.Cells[2].Value.ToString());
//    }

//try
//{
//    sql.ExecuteNonQuery();
//    invoiceID();
//    MessageBox.Show("ss");
//}
//catch (Exception ex)
//{
//    MessageBox.Show($"Error > {ex.Message}");
//}
//Con.Close();
