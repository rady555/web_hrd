﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace POS_Csharp.Forms
{
    public partial class frmUnit_Mearsure : Form
    {
        public frmUnit_Mearsure()
        {
            InitializeComponent();
            Reset();
            DisplayUM();
            Refresh();
            Count_rows();
        }

        SqlConnection Con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");

        private void DisplayUM()
        {
            Con.Open();
            String q = ("SELECT umid, umname, notes, active, factor FROM icum");
            SqlCommand sqli = new SqlCommand(q, Con);
            SqlDataAdapter sql = new SqlDataAdapter(q, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            while (reader.Read())
            {
                dgvUM.Rows.Add(reader["umid"], reader["umname"], reader["factor"], reader["notes"], reader["active"]);
                dgvUM.RowHeadersVisible = false;
                //count = dgvUM.RowCount;
                //lblCount.Text = count.ToString() + " records";
            }
            Con.Close();
        }
        public int Count_rows()
        {
            int count = 0;
            Con.Open();
            string stmt = "SELECT COUNT(*) FROM dbo.icum";
            SqlCommand cmdCount = new SqlCommand(stmt, Con);
            count = (int)cmdCount.ExecuteScalar();
            Con.Close();
            lblCount.Text = count.ToString() + " records";
            return count;
        }

        private void Refresh()
        {
            Con.Open();
            String q = ("SELECT umid, umname, notes, active, factor FROM icum WHERE umid like'" + this.txtSearch.Text + "%' OR umname like'" + this.txtSearch.Text + "%'");
            SqlCommand sqli = new SqlCommand(q, Con);
            SqlDataAdapter sql = new SqlDataAdapter(q, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            dgvUM.Rows.Clear();
            while (reader.Read())
            {
                dgvUM.Rows.Add(reader["umid"], reader["umname"], reader["factor"], reader["notes"], reader["active"]);
                dgvUM.RowHeadersVisible = false;
            }
            Con.Close();
        }


        private void Reset()
        {
            txtumid.Text = "";
            txtumname.Text = "";
            txtFactor.Text = "";
            checkActive.Checked = false;
            txtNote.Text = "";
        }

        int Key = 0;
        private void dgvUM_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                txtumid.Text = dgvUM.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtumname.Text = dgvUM.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtFactor.Text = dgvUM.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtNote.Text = dgvUM.Rows[e.RowIndex].Cells[3].Value.ToString();
                if (txtumid.Text == "")
                {
                    Key = 0;
                }
                else
                {
                    Key = Convert.ToInt32(dgvUM.Rows[e.RowIndex].Cells[0].Value.ToString());
                }

                if (dgvUM.Rows[e.RowIndex].Cells[4].Value.ToString() == "1")
                {
                    checkActive.Checked = true;
                }
                else
                {
                    checkActive.Checked = false;
                }
            }
            else
            {
                Reset();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Key == 0)
            {
                MessageBox.Show("Please select any row or column you want to delete!");
            }
            else
            {
                if (MessageBox.Show("Do you want to delete this record?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {

                    Con.Open();
                    SqlCommand cmd = new SqlCommand("delete FROM icum where umid = @umid", Con);
                    cmd.Parameters.AddWithValue("@umid", txtumid.Text);

                    cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    Con.Close();
                    MessageBox.Show("Unit of Measure Deleted!");
                    Reset();
                    Refresh();
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("update icum set umid=@umid, umname=@umname, notes=@notes, active=@active, factor=@factor where umid=@PKey;", Con);
            Con.Open();
            int active = 0;
            if (checkActive.Checked == true)
            {
                active = 1;
            }
            cmd.Parameters.AddWithValue("@umid", txtumid.Text.ToString());
            cmd.Parameters.AddWithValue("@umname", txtumname.Text);
            cmd.Parameters.AddWithValue("@notes", txtNote.Text.ToString());
            cmd.Parameters.AddWithValue("@active", active);
            cmd.Parameters.AddWithValue("@factor", txtFactor.Text);
            cmd.Parameters.AddWithValue("@PKey", Key);

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Unit of Measure Updated!");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Con.Close();
            Refresh();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new frmAddUM(dgvUM).Show();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
