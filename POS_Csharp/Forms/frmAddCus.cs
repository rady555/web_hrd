﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;


namespace POS_Csharp.Forms
{
    public partial class frmAddCus : Form
    {
        private DataGridView dgvCus = null;

        public frmAddCus(DataGridView dgvCus)
        {
            InitializeComponent();
            this.dgvCus = dgvCus;
            cateid();
            Refresh();
        }

        SqlConnection con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);


        protected override void WndProc(ref Message m)
        {
            const int WM_NCCALCSIZE = 0x0083;
            if (m.Msg == WM_NCCALCSIZE && m.WParam.ToInt32() == 1)
            {
                return;
            }
            base.WndProc(ref m);
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtCustomerName.Clear(); 
            txtPhone.Clear(); 
            txtAddress.Clear(); 
            checkActive.Checked = false;
        }

        public void cateid()
        {
            con.Open();
            int i = 0;
            string query = "select top 1 cusid from poscust order by cusid desc";
            SqlCommand cc = new SqlCommand(query, con);
            SqlDataReader dr = cc.ExecuteReader();
            while (dr.Read())
            {
                i = int.Parse(dr["cusid"].ToString());
            }
            ++i;
            txtCustomerID.Text = i.ToString();
            //Console.WriteLine(i);
            dr.Close();
            con.Close();

        }
        private void Reset()
        {
            // txtCustomerID.Text = "";
            txtCustomerName.Text = "";
            txtPhone.Text = "";
            txtAddress.Text = "";
            checkActive.Checked = false;
        }

        private void Refresh()
        {
            con.Open();
            String q = ("SELECT cusid, cusname, address, phone,active FROM poscust");
            SqlCommand sqli = new SqlCommand(q, con);
            SqlDataAdapter sql = new SqlDataAdapter(q, con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            dgvCus.Rows.Clear();
            while (reader.Read())
            {
                dgvCus.Rows.Add(reader["cusid"], reader["cusname"], reader["phone"], reader["address"], reader["active"]);
                dgvCus.RowHeadersVisible = false;
            }
            con.Close();
        }


        int key = 0;

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtCustomerName.Text == "")
            {
                key = 0;
                MessageBox.Show("Please input customer name!");
            }
            else if (txtPhone.Text == "")
            {
                key = 0;
                MessageBox.Show("Please input customer phone number!");
            }
            else if (txtAddress.Text == "")
            {
                key = 0;
                MessageBox.Show("Please input customer address!");
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");
                con.Open();
                SqlCommand cmd = new SqlCommand("insert into poscust (cusid, cusname, address, phone,active) values (@cusid, @cusname, @address, @phone,@active)", con);
                int active = 0;
                if (checkActive.Checked == true)
                {
                    active = 1;
                }
                cmd.Parameters.AddWithValue("@cusid", txtCustomerID.Text);
                cmd.Parameters.AddWithValue("@cusname", txtCustomerName.Text);
                cmd.Parameters.AddWithValue("@address", txtAddress.Text);
                cmd.Parameters.AddWithValue("@phone", txtPhone.Text);
                cmd.Parameters.AddWithValue("@active", active);


                try
                {
                    cmd.ExecuteNonQuery();
                    //cateid();
                    //MessageBox.Show("Customer Added!");
                    //Reset();
                    //Refresh();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                if (MessageBox.Show("Do you want to add more customer?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Reset();
                    Refresh();
                    cateid();
                }
                else
                {
                    this.Close();
                    Refresh();
                    con.Close();
                }
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
    }
}
