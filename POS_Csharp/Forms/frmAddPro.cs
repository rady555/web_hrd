﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;


namespace POS_Csharp.Forms
{
    public partial class frmAddPro : Form
    {
        private DataGridView dgvPro = null;
        SqlCommand cmd;

        public frmAddPro(DataGridView dgvPro)
        {
            InitializeComponent();
            this.dgvPro = dgvPro;
            Reset();
            Refresh();
            DisplayPro();
        }
        SqlConnection con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);


        protected override void WndProc(ref Message m)
        {
            const int WM_NCCALCSIZE = 0x0083;
            if (m.Msg == WM_NCCALCSIZE && m.WParam.ToInt32() == 1)
            {
                return;
            }
            base.WndProc(ref m);
        }

        private void Reset()
        {
            txtProID.Text = "";
            txtProName.Text = "";
            cboCateID.Text = "";
            txtNote.Text = "";
            txtPrice.Text = "";
            cboumid.Text = "";
            cbosumid.Text = "";
            checkActive.Checked = false;
        }
        private void DisplayPro()
        {
            con.Open();
            String q = ("SELECT productid,image, productname, categoryid,active, notes, price, umid, sumid FROM icproduct");
            SqlCommand sqli = new SqlCommand(q, con);
            SqlDataAdapter sql = new SqlDataAdapter(q, con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            while (reader.Read())
            {
                dgvPro.Rows.Add(reader["productid"], reader["image"], reader["productname"], reader["price"], reader["categoryid"], reader["umid"], reader["sumid"], reader["notes"], reader["active"]);
                dgvPro.RowHeadersVisible = false;
            }
            con.Close();
        }
        private void Refresh()
        {
            con.Open();
            String q = ("SELECT productid,image, productname, categoryid,active, notes, price, umid, sumid FROM icproduct");
            SqlCommand sqli = new SqlCommand(q, con);
            SqlDataAdapter sql = new SqlDataAdapter(q, con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sql);
            var ds = new DataSet();
            var reader = sqli.ExecuteReader();
            dgvPro.Rows.Clear();
            while (reader.Read())
            {
                dgvPro.Rows.Add(reader["productid"], reader["image"], reader["productname"], reader["price"], reader["categoryid"], reader["umid"], reader["sumid"], reader["notes"], reader["active"]);
                dgvPro.RowHeadersVisible = false;
            }
            con.Close();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        int key = 0;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            int active = 0;
            if (checkActive.Checked)
            {
                active = 1;
            }
            try
            {
                if (txtProID.Text == "" || txtProName.Text == "" || txtPrice.Text == "")
                {
                    MessageBox.Show("Missing");
                }
                else
                {
                    MemoryStream ms = new MemoryStream();
                    if (imagePro.Image != null)
                    {
                        cmd = new SqlCommand("insert into icproduct (productid, productname, categoryid,active, notes, price, umid, sumid, image, created_at) " +
                        "values(@productid, @productname, @categoryid,@active, @notes,@price, @umid, @sumid, @image,@created_at)", con);
                        imagePro.Image.Save(ms, imagePro.Image.RawFormat);
                        cmd.Parameters.AddWithValue("@image", ms.ToArray());
                    }
                    else
                    {
                        cmd = new SqlCommand("insert into icproduct (productid, productname, categoryid,active, notes, price, umid, sumid,created_at) " +
                        "values(@productid, @productname, @categoryid,@active, @notes,@price, @umid, @sumid,@created_at)", con);

                    }

                    cmd.Parameters.AddWithValue("@productid", txtProID.Text);
                    cmd.Parameters.AddWithValue("@productname", txtProName.Text);
                    cmd.Parameters.AddWithValue("@categoryid", cboCateID.Text);
                    cmd.Parameters.AddWithValue("@active", active);
                    cmd.Parameters.AddWithValue("@notes", txtNote.Text);
                    cmd.Parameters.AddWithValue("@price", txtPrice.Text);
                    cmd.Parameters.AddWithValue("@umid", cboumid.Text);
                    cmd.Parameters.AddWithValue("@sumid", cbosumid.Text);
                    cmd.Parameters.AddWithValue("@created_at", date.Value);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    
                    cmd.Parameters.Clear();
                    //MessageBox.Show("Inserted Successfully!");
                    //Reset();
                    //Refresh();
                    if (MessageBox.Show("Do you want to add more product?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Reset();
                        Refresh();
                    }
                    else
                    {
                        this.Close();
                        con.Close();
                        Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtProID.Clear();
            imagePro.Image = null;
            if (imagePro.Image == null)
            {
                imagePro.Image = Image.FromFile(@"C:\Users\radyk\Downloads\no-image.png");
                imagePro.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            txtProName.Clear();
            cboCateID.Text = "";
            txtNote.Clear();
            txtPrice.Clear();
            cboumid.Text = "";
            cbosumid.Text = "";
            checkActive.Checked = false;
        }

        private void checkActive_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtCateID_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCategoryName_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtNote_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnPic_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Select  image(*.Jpg; *.png; *.Gif)|*.Jpg; *.png; *.Gif";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                imagePro.Image = Image.FromFile(openFileDialog1.FileName);
            }
        }

        private void date_ValueChanged(object sender, EventArgs e)
        {

        }

        private void frmAddPro_Load(object sender, EventArgs e)
        {
            if (imagePro.Image == null)
            {
                imagePro.Image = Image.FromFile(@"C:\Users\radyk\Downloads\no-image.png");
                imagePro.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }
    }
}
