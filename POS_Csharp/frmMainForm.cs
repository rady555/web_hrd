
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;
using System.Data.SqlClient;

namespace POS_Csharp
{

    public partial class frmMainForm : Form
    {
        public static frmMainForm instance;
        public Button btnSale_Ins;
        public Button btnPro_Ins;
        public Button btnCate_ins;
        public Button btnCus_Ins;
        public Button btnUser_ins;

        private int borderSize = 2;
        private Form activeForm;
        private IconButton currentBtn;

        public frmMainForm()
        {
            InitializeComponent();

            CollapseMenu();
            this.Padding = new Padding(borderSize);
            // this.borderSize = Color.FromArgb(47, 70, 79);
            ActivateBtn(btnDash, RGBColors.color8);
            //CollapseMenu();
            OpenChildForm(new Forms.Dashboard(), btnDash);
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);

        SqlConnection con = new SqlConnection(@"Data Source=USER\SQLEXPRESS;Initial Catalog=Blue;Integrated Security=True;");


        private void frmMainForm_Load(object sender, EventArgs e)
        {
            //searchData("");
            date_timer.Start();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        protected override void WndProc(ref Message m)
        {
            const int WM_NCCALCSIZE = 0x0083;
            if(m.Msg == WM_NCCALCSIZE && m.WParam.ToInt32() == 1)
            {
                return;
            }
            base.WndProc(ref m);
        }

        private void frmMainForm_Resize(object sender, EventArgs e)
        {
            AdjustForm();
        }
        private void AdjustForm()
        {
            switch (this.WindowState)
            {
                case FormWindowState.Maximized:
                    this.Padding = new Padding(0, 8, 8, 0);
                    break;
                case FormWindowState.Normal:
                    if (this.Padding.Top != borderSize)
                        this.Padding = new Padding(borderSize);
                    break;
            }
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMaximize_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
            else
                this.WindowState = FormWindowState.Normal;
                   //1426, 778
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //private void btnMenu_Click(object sender, EventArgs e)
        //{
        //CollapseMenu();
        //}
        /*private void CollapseMenu()
        {
            if(this.panelMenu.Width > 200)
            {
                panelMenu.Width = 100;
                lblpos.Visible = false;
                btnMenu.Dock = DockStyle.Top;
                foreach(Button menuButton in panelMenu.Controls.OfType<Button>())
                {
                    menuButton.Text = "";
                    menuButton.ImageAlign = ContentAlignment.MiddleCenter;
                    menuButton.Padding = new Padding(0);
                }
            }
            else
            {
                panelMenu.Width = 300;
                lblpos.Visible = true;
                btnMenu.Dock= DockStyle.None;
                foreach(Button menuButton in panelMenu.Controls.OfType<Button>())
                {
                    menuButton.Text = "   " + menuButton.Tag.ToString();
                    menuButton.ImageAlign = ContentAlignment.MiddleLeft;
                    menuButton.Padding = new Padding(10, 0, 0, 0);
                }
            }
        }*/
        private void OpenChildForm(Form childForm, object btnSender)
        {
            if (activeForm != null)
            {
                activeForm.Hide();
            }
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panelDesktop.Controls.Add(childForm);
            this.panelDesktop.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            lblNameButton.Text = childForm.Text;
            //lblpos.Text = childForm.Text;
        }

        private struct RGBColors
        {
            public static Color color1 = Color.FromArgb(172, 126, 241);
            public static Color color2 = Color.FromArgb(249, 118, 176);
            public static Color color3 = Color.FromArgb(253, 138, 114);
            public static Color color4 = Color.FromArgb(95, 77, 221);
            public static Color color5 = Color.FromArgb(249, 88, 155);
            public static Color color6 = Color.FromArgb(24, 161, 251);
            public static Color color7 = Color.FromArgb(0, 0, 0);
            public static Color color8 = Color.FromArgb(0, 0, 255);
        }

        private void ActivateBtn(object senderBtn, Color color)
        {
            
            if (senderBtn != null)
            {
                DisableBtn();
                currentBtn = (IconButton)senderBtn;
                //currentBtn.BackColor = Color.FromArgb(255, 255, 255);
                currentBtn.BackColor = Color.FromArgb(255, 255, 255);
                currentBtn.ForeColor = color;
                currentBtn.TextAlign = ContentAlignment.MiddleLeft;
                currentBtn.IconColor = color;
               // currentBtn.TextImageRelation = TextImageRelation.TextBeforeImage;
                currentBtn.ImageAlign = ContentAlignment.MiddleLeft;

            }
        }

        private void DisableBtn()
        {
            if (currentBtn != null)
            {
                currentBtn.BackColor = Color.FromArgb(47, 79, 79);
                //currentBtn.ForeColor = Color.Gainsboro;
                currentBtn.ForeColor = Color.White;
                currentBtn.TextAlign = ContentAlignment.MiddleLeft;
                //currentBtn.IconColor = Color.Gainsboro;
                currentBtn.ForeColor = Color.White;
                currentBtn.IconColor = Color.White;
                //currentBtn.TextImageRelation = TextImageRelation.ImageBeforeText;
                currentBtn.ImageAlign = ContentAlignment.MiddleLeft;
            }
        }

        private void btnUser_Click(object sender, EventArgs e)
        {
            ActivateBtn(sender, RGBColors.color8);
            //CollapseMenu();
            OpenChildForm(new Forms.frmUser(), sender);           
        }

        private void btnCus_Click(object sender, EventArgs e)
        {
            ActivateBtn(sender, RGBColors.color8);
            //CollapseMenu();
            OpenChildForm(new Forms.frmCustomer(), sender);

        }

        private void btnPro_Click(object sender, EventArgs e)
        {
            ActivateBtn(sender, RGBColors.color8);
            //CollapseMenu();
            OpenChildForm(new Forms.frmProduct(), sender);
        }

        private void btnCate_Click(object sender, EventArgs e)
        {
            ActivateBtn(sender, RGBColors.color8);
            //CollapseMenu();
            OpenChildForm(new Forms.frmCategory(), sender);

        }

        private void btnSale_Click(object sender, EventArgs e)
        {
            ActivateBtn(sender, RGBColors.color8);
            //CollapseMenu();
            OpenChildForm(new Forms.frmSale(), sender);
        }

        private void btnUM_Click(object sender, EventArgs e)
        {
            ActivateBtn(sender, RGBColors.color8);
            //CollapseMenu();
            OpenChildForm(new Forms.frmUnit_Mearsure(), sender);
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            ActivateBtn(sender, RGBColors.color8);
            if (MessageBox.Show("Do you want to logout?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                new frmLog_In().Show();
            this.Hide();
            //System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(LoginForm));
            //t.Start();
            //this.Close();
        }
        public static void LoginForm()
        {
            Application.Run(new frmLog_In());
        }
        /*public void searchData(string valueToSearch)
        {
            try
            {
                string sql = "SELECT userid, email, username, gender, password, address, active, phone, date_of_birth FROM sysuser WHERE " +
                "CONCAT(email, username, date_of_birth) LIKE '%" + valueToSearch + "%'";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataAdapter adt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adt.Fill(dt);
                dgv1.DataSource = dt;
            }
            catch (Exception ex)
            {
                Refresh();
                MessageBox.Show(ex.Message);
            }
        }*/

        private void btnSearch_Click(object sender, EventArgs e)
        {
            //string valueToSearch = txtSearch.Text.ToString();
            //searchData(valueToSearch);

        }

        private void date_timer_Tick(object sender, EventArgs e)
        {
            date.Text = DateTime.Now.ToLongDateString();
            time.Text = DateTime.Now.ToLongTimeString();
        }

        private void panelDesktop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Menu_Click(object sender, EventArgs e)
        {
            CollapseMenu();
        }
        private void CollapseMenu()
        {
            if (this.panelMenu.Width > 200)
            {
                panelMenu.Width = 80;
                lblPos.Visible = false;
                Menu.Dock = DockStyle.Top;
                Menu.Dock = DockStyle.None;
                foreach (Button menuButton in panelMenu.Controls.OfType<Button>())
                {
                    menuButton.Text = "";
                    menuButton.ImageAlign = ContentAlignment.MiddleLeft;
                    menuButton.Padding = new Padding(10, 0, 0, 0);
                }
            }
            else
            {
                panelMenu.Width = 345;
                lblPos.Visible = true;
                Menu.Dock = DockStyle.None;
                foreach (Button menuButton in panelMenu.Controls.OfType<Button>())
                {
                    menuButton.Text = "   " + menuButton.Tag.ToString();
                    menuButton.ImageAlign = ContentAlignment.MiddleLeft;
                    menuButton.Padding = new Padding(30, 0, 0, 0);
                }
            }
        }

        private void lblPos_Click(object sender, EventArgs e)
        {

        }

        private void iconButton1_Click(object sender, EventArgs e)
        {

        }

        private void btnDash_Click(object sender, EventArgs e)
        {
            ActivateBtn(sender, RGBColors.color8);
            OpenChildForm(new Forms.Dashboard(), sender);
        }
    }
}